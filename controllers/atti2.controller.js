const models = require('../models/models');

/* 태이블 정보
    writer: sequelize.INTEGER,
    status: Sequelize.INTEGER,
    memo: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
  const companyindex = parseInt(req.params.companyindex, 10);
  // const userid = req.params.userid;
  if (!companyindex) {
    return res.status(400).json({error: 'Incorrect id'});
  }

  // console.log(userindex);
  models.attiBoard.findAll({
    where: {
      companyid: companyindex
    }
  }).then(attiBoard => {
    if (!attiBoard) {
      return res.status(404).json({error: 'No attiBoard'});
    }

    return res.json(
      {
        'data' : attiBoard
      }
    )});
};

// 내 기록 보기
exports.show = (req, res) => {
  const userindex = parseInt(req.params.id, 10);
  // const userid = req.params.userid;
  if (!userindex) {
    return res.status(400).json({error: 'Incorrect id'});
  }

  // console.log(userindex);
  models.attiBoard.findAll({
    where: {
      userindex: userindex
    }
  }).then(attiBoard => {
    if (!attiBoard) {
      return res.status(404).json({error: 'No attiBoard'});
    }

    return res.json(
      {
        'data' : attiBoard
      }
    )});
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.attiBoard.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const userindex= req.body.userindex || '';
  const userid= req.body.userid || '';
  const writer= req.body.writer || '';
  const status= req.body.status || '';
  const memo= req.body.memo || '';

  if (!userid) {
    return res.status(400).json({error: 'Incorrect id'});
  }

    models.attiBoard.create({
      userindex: userindex,
      userid: userid,
      writer: writer,
      status: status,
      memo: memo
    },
  ).then((data) => res.status(201).json(data))
};

// 업데이트
exports.update = (req, res) => {
  const id = req.params.id;
  const body = req.body;

  // const userid = body.userid || '0';
  const online = body.online || '0';

  // console.log(online);

  models.User.update(
    {
      online: online
    }, 
    {
      where: 
      {
        userid:id
      }
    })
    .then((data) => res.status(200).json(data))
    .then(() => req.session.passport.user.online = online)
  }

  