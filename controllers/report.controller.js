const models = require('../models/models');

/* 태이블 정보
    writer: sequelize.INTEGER,
    title: Sequelize.STRING,
    thismon: sequelize.STRING,
    thistue: sequelize.STRING,
    thiswed: sequelize.STRING,
    thisthu: sequelize.STRING,
    thisfri: sequelize.STRING,
    thisetc: sequelize.STRING,
    nextmon: sequelize.STRING,
    nexttue: sequelize.STRING,
    nextwed: sequelize.STRING,
    nextthu: sequelize.STRING,
    nextfri: sequelize.STRING,
    nextetc: sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.reportBoard.findAll({
      order: [['createdAt', 'DESC']]
    }).then(data => res.json(
          {
            'data' : data
          }
        ));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.reportBoard.findOne({
      where: {
        id: id
      }
    }).then(reportboard => {
      if (!reportboard) {
        return res.status(404).json({error: 'No reportboard'});
      }
  
      return res.json(reportboard);
    });
};



// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.reportBoard.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const userindex= req.body.userindex;
  const writer= req.body.writer;
  const userid= req.body.userid;
  const title= req.body.title || '';
  const companyid= req.body.companyid || '';
  const companyname= req.body.companyname || '';

  const year = req.body.year || '';
  const month = req.body.month || '';
  const week = req.body.week || '';

  const thismon= req.body.thismon || '';
  const thistue= req.body.thistue || '';
  const thiswed= req.body.thiswed || '';
  const thisthu= req.body.thisthu || '';
  const thisfri= req.body.thisfri || '';
  const thisetc= req.body.thisetc || '';

  const filepath= req.body.filepath || '';

  const startday= req.body.startday || '';
  const endday= req.body.endday || '';

    //if (!writer.length) {
    //  return res.status(400).json({error: 'Incorrenct writer'});
    //}
  
    models.reportBoard.create({
      userindex: userindex,
      userid: userid,
      writer: writer,
      title: year + "년 " + month + "월 " + week + "주차 업무보고",
      companyid: companyid,
      companyname: companyname,

      year: year,
      month: month,
      week: week,

      thismon: thismon,
      thistue: thistue,
      thiswed: thiswed,
      thisthu: thisthu,
      thisfri: thisfri,
      thisetc: thisetc,

      filepath: filepath,

      startday: startday,
      endday: endday
    }).then((reportboard) => res.status(201).json(reportboard))
};

// 업데이트
exports.update = (req, res) => {
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  const thismon= body.thismon || '';
  const thistue= body.thistue || '';
  const thiswed= body.thiswed || '';
  const thisthu= body.thisthu || '';
  const thisfri= body.thisfri || '';
  const thisetc= body.thisetc || '';
  
  const nextmon= body.nextmon || '';
  const nexttue= body.nexttue || '';
  const nextwed= body.nextwed || '';
  const nextthu= body.nextthu || '';
  const nextfri= body.nextfri || '';
  const nextetc= body.nextetc || '';

  models.reportBoard.update(
    {
      thismon: thismon,
      thistue: thistue,
      thiswed: thiswed,
      thisthu: thisthu,
      thisfri: thisfri,
      thisetc: thisetc,

      nextmon: nextmon,
      nexttue: nexttue,
      nextwed: nextwed,
      nextthu: nextthu,
      nextfri: nextfri,
      nextetc: nextetc
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((data) => res.status(200).json(data))
  }

  