const models = require('../models/models');

/* 태이블 정보
    writer: sequelize.INTEGER,
    title: Sequelize.STRING,
    thismon: sequelize.STRING,
    thistue: sequelize.STRING,
    thiswed: sequelize.STRING,
    thisthu: sequelize.STRING,
    thisfri: sequelize.STRING,
    thisetc: sequelize.STRING,
    nextmon: sequelize.STRING,
    nexttue: sequelize.STRING,
    nextwed: sequelize.STRING,
    nextthu: sequelize.STRING,
    nextfri: sequelize.STRING,
    nextetc: sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
  const userindex = req.params.userindex;

  // const userid = req.params.userid;
  if (!userindex) {
    return res.status(400).json({error: 'Incorrect userindex'});
  }

  // console.log(userindex);
  models.reportBoard.findAll({
    where: {
      userindex: userindex
    }
  }).then(data => {
    if (!data) {
      return res.status(404).json({error: 'No data'});
    }

    return res.status(200).json(
      {
        'data' : data
      }
    )});
};
  
// 내 기록 보기
exports.show = (req, res) => {
  const userid = req.params.useris;
  const year = parseInt(req.params.year, 10);
  const month = parseInt(req.params.month, 10);
  const week = parseInt(req.params.week, 10);

  // const userid = req.params.userid;
  if (!userid) {
    return res.status(400).json({error: 'Incorrect id'});
  }

  // console.log(userindex);
  models.reportBoard.findOne({
    where: {
      userid: userid,
      year: year,
      month: month,
      week: week
    }
  }).then(data => {
    if (!data) {
      return res.status(404).json({error: 'No data'});
    }

    return res.status(200).json(
      {
        'data' : data
      }
    )});
};



// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.reportBoard.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const writer= req.body.writer;
  const userid= req.body.userid;
  const title= req.body.title || '';

  const thismon= req.body.thismon || '';
  const thistue= req.body.thistue || '';
  const thiswed= req.body.thiswed || '';
  const thisthu= req.body.thisthu || '';
  const thisfri= req.body.thisfri || '';
  const thisetc= req.body.thisetc || '';
  
  const nextmon= req.body.nextmon || '';
  const nexttue= req.body.nexttue || '';
  const nextwed= req.body.nextwed || '';
  const nextthu= req.body.nextthu || '';
  const nextfri= req.body.nextfri || '';
  const nextetc= req.body.nextetc || '';

    //if (!writer.length) {
    //  return res.status(400).json({error: 'Incorrenct writer'});
    //}
  
    models.reportBoard.create({
      userid: userid,
      writer: writer,
      title: title,

      thismon: thismon,
      thistue: thistue,
      thiswed: thiswed,
      thisthu: thisthu,
      thisfri: thisfri,
      thisetc: thisetc,

      nextmon: nextmon,
      nexttue: nexttue,
      nextwed: nextwed,
      nextthu: nextthu,
      nextfri: nextfri,
      nextetc: nextetc
    }).then((reportboard) => res.status(201).json(reportboard))
};

// 업데이트
exports.update = (req, res) => {
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  const title= body.title || '';

  const thismon= body.thismon || '';
  const thistue= body.thistue || '';
  const thiswed= body.thiswed || '';
  const thisthu= body.thisthu || '';
  const thisfri= body.thisfri || '';
  const thisetc= body.thisetc || '';
  
  const nextmon= body.nextmon || '';
  const nexttue= body.nexttue || '';
  const nextwed= body.nextwed || '';
  const nextthu= body.nextthu || '';
  const nextfri= body.nextfri || '';
  const nextetc= body.nextetc || '';

  models.reportBoard.update(
    {
      title: title,

      thismon: thismon,
      thistue: thistue,
      thiswed: thiswed,
      thisthu: thisthu,
      thisfri: thisfri,
      thisetc: thisetc,

      nextmon: nextmon,
      nexttue: nexttue,
      nextwed: nextwed,
      nextthu: nextthu,
      nextfri: nextfri,
      nextetc: nextetc
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((data) => res.status(200).json(data))
  }

  