const models = require('../models/models');
var Excel = require('exceljs');
const fs = require('fs');
const mime = require('mime-types');
const path = require('path');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    userid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
  models.User.findAll({
    where: {
      admin: 0
    }
  }).then(data => {
    if (!data) {
      return res.status(404).json({error: 'No data'});
    }

    var workbook = new Excel.Workbook();
    workbook.creator = 'Keeplog';
    workbook.lastModifiedBy = 'Keeplog';
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.lastPrinted = new Date();

    var filename = data[0].companyname + "_근로자현황.xlsx";
    var savepath = path.join(__dirname, "..", "public", "temp", filename);
    var sheet = workbook.addWorksheet('My Sheet', {
      pageSetup:{paperSize: 9, orientation:'landscape'}
    });
    var worksheet = workbook.getWorksheet(1);

    // 제목
    worksheet.mergeCells('E3:I4');
    worksheet.getCell("I4").value = "근로자 현황";
    worksheet.getCell('I4').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('I4').font = {
        name: '맑은 고딕',
        family: 4,
        size: 20,
        bold: true
    };

    // 소속
    worksheet.getCell("A6").value = "회사 : ";
    worksheet.mergeCells('B6:D6');
    worksheet.getCell("D6").value = data[0].companyname;
    worksheet.getCell('D6').alignment = { vertical: 'middle', horizontal: 'center' };

    // 카운트
    worksheet.getCell("A7").value = " ";
    worksheet.getCell('A7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 이름
    worksheet.getCell("B7").value = "이 름";
    worksheet.getCell('B7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 연락처
    worksheet.mergeCells('C7:G7');
    worksheet.getCell("G7").value = "주소";
    worksheet.getCell('G7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('G7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 연락처
    worksheet.mergeCells('H7:I7');
    worksheet.getCell("I7").value = "연락처";
    worksheet.getCell('I7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('I7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 장애등급
    worksheet.getCell("J7").value = "장애등급";
    worksheet.getCell('J7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('J7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 연락처
    worksheet.mergeCells('K7:L7');
    worksheet.getCell("L7").value = "입사일자";
    worksheet.getCell('L7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('L7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 입사일자
    worksheet.getCell("M7").value = "근무시간";
    worksheet.getCell('M7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('M7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

  for(var i=0, a=0; i<data.length*2; i+=2, a++)
  {
    // 숫자
    worksheet.mergeCells('A'+(i+8)+':A'+(i+9));
    worksheet.getCell("A"+(i+9)).value = a+1;
    worksheet.getCell('A'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 이름
    worksheet.mergeCells('B'+(i+8)+':B'+(i+9));
    worksheet.getCell("B"+(i+9)).value = data[a].name;
    worksheet.getCell('B'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    //  주소
    worksheet.mergeCells('C'+(i+8)+':G'+(i+9));
    worksheet.getCell("G"+(i+9)).value = data[a].address;
    worksheet.getCell('G'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('G'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 연락처
    worksheet.mergeCells('H'+(i+8)+':I'+(i+9));
    worksheet.getCell("I"+(i+9)).value = data[a].phone;
    worksheet.getCell('I'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('I'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };    

    // 장애등급
    worksheet.mergeCells('J'+(i+8)+':J'+(i+9));
    worksheet.getCell("J"+(i+9)).value = data[a].unuselevel + " 급";
    worksheet.getCell('J'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('J'+(i+9)).numFmt = '';
    worksheet.getCell('J'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 입사일자
    worksheet.mergeCells('K'+(i+8)+':L'+(i+8));
    worksheet.getCell("L"+(i+8)).value = data[a].inday;
    worksheet.getCell('L'+(i+8)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('L'+(i+8)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 퇴사일자
    worksheet.mergeCells('K'+(i+9)+':L'+(i+9));
    worksheet.getCell("L"+(i+9)).value = data[a].outday;
    worksheet.getCell('L'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('L'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 근무시간
    worksheet.mergeCells('M'+(i+8)+':M'+(i+9));
    worksheet.getCell("M"+(i+9)).value = data[a].worktime + " 시간";
    worksheet.getCell('M'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('M'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };
  }

  try
  { 
      if(path.join(__dirname, "..", "public", "temp") !== '')
          {
              lastfile = path.join(__dirname, "..", "public", "temp");
              fs.mkdirSync(lastfile);
              console.log("Make folder = " + lastfile);
          }
      else
          console.log('Folder is empty.');
  }
  catch(e)
  { 
      if ( e.code != 'EEXIST' ) 
          throw e; // 존재할경우 패스처리함. 
  }

    workbook.xlsx.writeFile(savepath)
    .then(data => {
      res.download(path.join(__dirname, "..", "public", "temp", filename));
    })
  })
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const companyid = parseInt(req.params.companyid, 10);
  console.log(companyid);
  if (!companyid) {
    return res.status(400).json({error: 'Incorrect companyid'});
  }

  models.User.findAll({
    where: {
      companyid: companyid,
      admin: 0
    }
  }).then(data => {
    if (!data) {
      return res.status(404).json({error: 'No data'});
    }

    var workbook = new Excel.Workbook();
    workbook.creator = 'Keeplog';
    workbook.lastModifiedBy = 'Keeplog';
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.lastPrinted = new Date();

    var filename = data[0].companyname + "_근로자현황.xlsx";
    var savepath = path.join(__dirname, "..", "public", "temp", filename);
    var sheet = workbook.addWorksheet('My Sheet', {
      pageSetup:{paperSize: 9, orientation:'landscape'}
    });
    var worksheet = workbook.getWorksheet(1);

    // 제목
    worksheet.mergeCells('E3:I4');
    worksheet.getCell("I4").value = "근로자 현황";
    worksheet.getCell('I4').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('I4').font = {
        name: '맑은 고딕',
        family: 4,
        size: 20,
        bold: true
    };

    // 소속
    worksheet.getCell("A6").value = "회사 : ";
    worksheet.mergeCells('B6:D6');
    worksheet.getCell("D6").value = data[0].companyname;
    worksheet.getCell('D6').alignment = { vertical: 'middle', horizontal: 'center' };

    // 카운트
    worksheet.getCell("A7").value = " ";
    worksheet.getCell('A7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 이름
    worksheet.getCell("B7").value = "이 름";
    worksheet.getCell('B7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 연락처
    worksheet.mergeCells('C7:G7');
    worksheet.getCell("G7").value = "주소";
    worksheet.getCell('G7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('G7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 연락처
    worksheet.mergeCells('H7:I7');
    worksheet.getCell("I7").value = "연락처";
    worksheet.getCell('I7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('I7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 장애등급
    worksheet.getCell("J7").value = "장애등급";
    worksheet.getCell('J7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('J7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 연락처
    worksheet.mergeCells('K7:L7');
    worksheet.getCell("L7").value = "입사일자";
    worksheet.getCell('L7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('L7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

    // 입사일자
    worksheet.getCell("M7").value = "근무시간";
    worksheet.getCell('M7').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('M7').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
  };

  for(var i=0, a=0; i<data.length*2; i+=2, a++)
  {
    // 숫자
    worksheet.mergeCells('A'+(i+8)+':A'+(i+9));
    worksheet.getCell("A"+(i+9)).value = a+1;
    worksheet.getCell('A'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 이름
    worksheet.mergeCells('B'+(i+8)+':B'+(i+9));
    worksheet.getCell("B"+(i+9)).value = data[a].name;
    worksheet.getCell('B'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    //  주소
    worksheet.mergeCells('C'+(i+8)+':G'+(i+9));
    worksheet.getCell("G"+(i+9)).value = data[a].address;
    worksheet.getCell('G'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('G'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 연락처
    worksheet.mergeCells('H'+(i+8)+':I'+(i+9));
    worksheet.getCell("I"+(i+9)).value = data[a].phone;
    worksheet.getCell('I'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('I'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };    

    // 장애등급
    worksheet.mergeCells('J'+(i+8)+':J'+(i+9));
    worksheet.getCell("J"+(i+9)).value = data[a].unuselevel + " 급";
    worksheet.getCell('J'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('J'+(i+9)).numFmt = '';
    worksheet.getCell('J'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 입사일자
    worksheet.mergeCells('K'+(i+8)+':L'+(i+8));
    worksheet.getCell("L"+(i+8)).value = data[a].inday;
    worksheet.getCell('L'+(i+8)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('L'+(i+8)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 퇴사일자
    worksheet.mergeCells('K'+(i+9)+':L'+(i+9));
    worksheet.getCell("L"+(i+9)).value = data[a].outday;
    worksheet.getCell('L'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('L'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };

    // 근무시간
    worksheet.mergeCells('M'+(i+8)+':M'+(i+9));
    worksheet.getCell("M"+(i+9)).value = data[a].worktime + " 시간";
    worksheet.getCell('M'+(i+9)).alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('M'+(i+9)).border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };
  }

  try
  { 
      if(path.join(__dirname, "..", "public", "temp") !== '')
          {
              lastfile = path.join(__dirname, "..", "public", "temp");
              fs.mkdirSync(lastfile);
              console.log("Make folder = " + lastfile);
          }
      else
          console.log('Folder is empty.');
  }
  catch(e)
  { 
      if ( e.code != 'EEXIST' ) 
          throw e; // 존재할경우 패스처리함. 
  }

    workbook.xlsx.writeFile(savepath)
    .then(data => {
      res.download(path.join(__dirname, "..", "public", "temp", filename));
    })
  })
};


// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.User.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const name = req.body.name || '';
    const userid = req.body.userid || '';
    const email = req.body.email || '';
    const phone = req.body.phone || '';
    const password = req.body.password || '';
    const companyid = req.body.companyid || '';
    const team = req.body.team || '';
    const admin = req.body.admin || '';

    if (!name.length) {
      return res.status(400).json({error: 'Incorrenct name'});
    }
  
    models.User.create({
      name: name,
      userid: userid,
      email: email,
      phone: phone,
      password: password,
      companyid: companyid,
      team: team,
      admin: admin
    }).then((user) => res.status(201).json(user))
};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  const name = body.name || '';
  const userid = body.userid || '';
  const email = body.email || '';
  const phone = body.phone || '';
  const password = body.password || '';
  const companyid = parseInt(body.companyid, 10);
  const team = body.team || '';  
  
  models.User.update(
    {
      name: name,
      userid: userid,
      email: email,
      phone: phone,
      password: password,
      companyid: companyid,
      team: team
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((user) => res.status(200).json(user))
      
  }

  