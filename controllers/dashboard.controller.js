const models = require('../models/models');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    userid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    const queryStr = "";
    query += "(SELECT count(*) as data FROM users as userCount)"; //전체
    query += "union";
    query += "(SELECT count(distinct writer) as data from attiboards where createdAt >= curdate() and time(createdAt) < \"09:00:00\" and status = '출근')"; // 출근
    query += "union";
    query += "(SELECT count(distinct writer) as data from attiboards where createdAt >= curdate() and time(createdAt) > \"09:00:00\" and status = '출근')"; // 지각

    models.sequelize.query(queryStr, { type: sequelize.QueryTypes.SELECT})
    .then(data => {
      return res.json(data);
    });
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const userid = parseInt(req.params.userid, 10);
  if (!userid) {
    return res.status(400).json({error: 'Incorrect userid'});
  }

  models.User.findOne({
    where: {
      userid: userid
    }
  }).then(user => {
    if (!user) {
      return res.status(404).json({error: 'No User'});
    }

    return res.json(user);
  });
};

// 조건 찾기
exports.show = (req, res) => {
    const companyid = parseInt(req.params.companyid, 10);
    if (!companyid) {
      return res.status(400).json({error: 'Incorrect companyid'});
    }
  
    models.User.findAll({
      where: {
        companyid: companyid
      }
    }).then(user => {
      if (!user) {
        return res.status(404).json({error: 'No User'});
      }
  
      return res.json(
        {
          'data' : data
        }
      );
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.User.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const name = req.body.name || '';
    const userid = req.body.userid || '';
    const email = req.body.email || '';
    const phone = req.body.phone || '';
    const password = req.body.password || '';
    const companyid = req.body.companyid || '';
    const team = req.body.team || '';
    const admin = req.body.admin || '';

    if (!name.length) {
      return res.status(400).json({error: 'Incorrenct name'});
    }
  
    models.User.create({
      name: name,
      userid: userid,
      email: email,
      phone: phone,
      password: password,
      companyid: companyid,
      team: team,
      admin: admin
    }).then((user) => res.status(201).json(user))
};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  const name = body.name || '';
  const userid = body.userid || '';
  const email = body.email || '';
  const phone = body.phone || '';
  const password = body.password || '';
  const companyid = parseInt(body.companyid, 10);
  const team = body.team || '';  
  
  models.User.update(
    {
      name: name,
      userid: userid,
      email: email,
      phone: phone,
      password: password,
      companyid: companyid,
      team: team
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((user) => res.status(200).json(user))
      
  }

  