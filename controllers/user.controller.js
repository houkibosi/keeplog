const models = require('../models/models');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    userid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.User.findAll()
        .then(data => res.json(
          {
            'data' : data
          }
        ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const userid = parseInt(req.params.userid, 10);
  if (!userid) {
    return res.status(400).json({error: 'Incorrect userid'});
  }

  models.User.findOne({
    where: {
      userid: userid
    }
  }).then(user => {
    if (!user) {
      return res.status(404).json({error: 'No User'});
    }

    return res.json(user);
  });
};

// 조건 찾기
exports.show = (req, res) => {
    const companyid = parseInt(req.params.companyid, 10);
    if (!companyid) {
      return res.status(400).json({error: 'Incorrect companyid'});
    }
  
    models.User.findAll({
      where: {
        companyid: companyid
      }
    }).then(data => {
      if (!data) {
        return res.status(404).json({error: 'No User'});
      }
  
      return res.json(
        {
          'data' : data
        }
      );
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.User.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const name = req.body.name || '';
    const userid = req.body.userid || '';
    const email = req.body.email || '';
    const phone = req.body.phone || '';
    const password = req.body.password || '';
    const companyid = req.body.companyid || '';
    const companyname = req.body.companyname || '';
    const team = req.body.team || '';
    const admin = parseInt(req.body.admin, 10) || '0';
    const worktime = parseInt(req.body.worktime, 10) || '';
    const address = req.body.address || '';
    const inday = req.body.inday || '';
    const unuselevel = req.body.unuselevel || '';
    
    

    if (!name.length) {
      return res.status(400).json({error: 'Incorrenct name'});
    }
  
    models.User.create({
      name: name,
      userid: userid,
      email: email,
      phone: phone,
      password: password,
      companyid: companyid,
      companyname: companyname,
      team: team,
      admin: admin,
      worktime: worktime,
      address: address,
      inday: inday,
      unuselevel: unuselevel
    }).then((user) => res.status(201).json(user))
};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  
  const userid = body.userid;
  const name = body.name;
  const phone = body.phone;
  const password = body.password;
  const admin = body.admin;
  const address = body.address;
  const inday = body.inday;
  const outday = body.outday;
  const worktime = parseInt(body.worktime, 10);
  const unuselevel = parseInt(body.unuselevel, 10);

  models.User.update(
    {
      phone: phone,
      password: password,
      admin: admin,
      worktime: worktime,
      inday: inday,
      outday: outday,
      worktime: worktime,
      unuselevel: unuselevel
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((user) => res.status(200).json(user))
      
  }

  