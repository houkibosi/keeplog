const models = require('../models/models');
var Excel  = require ( 'exceljs' ) ; 
var request = require('request-promise');
var async = require('async');
const path = require('path');
const fs = require('fs');

var filename = '';

var year = '';
var month = '';


/* 태이블 정보
    writer: sequelize.INTEGER,
    status: Sequelize.INTEGER,
    memo: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.attiBoard.findAll()
        .then(data => res.json(
          {
            'data' : data
          }
        ));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    year = parseInt(req.params.year, 10);
    month = parseInt(req.params.month, 10) < 10 ? "0" + parseInt(req.params.month, 10) : parseInt(req.params.month, 10);

    // today.getMonth()+1 < 10 ? "0" + (today.getMonth()+1) : today.getMonth()+1

    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    let queryStr = "";
    queryStr += "select atti.*, users.name as username, users.companyname ";
    queryStr += "from attiboards atti, users users ";
    queryStr += "where atti.userindex = users.id and users.id = "+id;
    

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(attiboard => {
    if (!attiboard) {
        return res.status(404).json({error: 'No attiboard'});
    }
    
    // console.log(attiboard[0]);

    var today = new Date();
    // var year = today.getFullYear();
    // var month = today.getMonth()+1 < 10 ? "0" + (today.getMonth()+1) : today.getMonth()+1;
    // var month = "02";
    var day = today.getDate() < 10 ? "0" + (today.getDate()) : today.getDate();

    var lastDay = ( new Date( year, month, 0) ).getDate();
    filename = attiboard[0].companyname + '_' + attiboard[0].username + '_' + month + "월 출근기록.xlsx";

    var week = ['일', '월', '화', '수', '목', '금', '토'];
    var days = new Array();
    var json = '';
    // var workbook = XLSX.readFile("D:\\써포트라인\\git\\public\\template\\출근현황.xlsx");
    // var workbook = XLSX.readFile("D:\\써포트라인\\git\\public\\template\\test.xlsx");
    var workbook = new Excel.Workbook();
    workbook.creator = 'Me';
    workbook.lastModifiedBy = 'Her';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    // workbook.xlsx.readFile("D:\\써포트라인\\git\\public\\template\\출근현황.xlsx")

    var headers = {
        'TDCProjectKey': 'c0e32f8d-2c04-40ae-832f-fe3d5a2c34d6'
    }
    var options = {
        url: 'https://apis.sktelecom.com/v1/eventday/days?type=h&year='+year+'&month='+month,
        method: 'GET',
        headers: headers
    }

    console.log("request start");
    request(options, function (error, response, data) {
        if (!error && response.statusCode == 200) {
            var json = JSON.parse(data);
            var holiday = new Array();
            // console.log(data);
            // for(i=0; i<json.totalResult; i++)
            //     holiday[i] = json.results[i];

            for(var a=1; a<=lastDay; a++)
            {
                // console.log("lastDay = " + lastDay);
                if(week[new Date(year + "-" + month + "-" +  a).getDay()] == "토" || week[new Date(year + "-" + month + "-" +  a).getDay()] == "일")
                {
                    // console.log(a + "번째 ")
                    days[a] = "휴일";
                }
                else
                {
                    // days[a] = "출근";
                    // console.log(attiboard);
                    
                    for(var z=0; z<attiboard.length; z++)
                    {
                        var day = new Date(attiboard[z].createdAt);
                        if(day.getDate() == a)
                        {
                            if(attiboard[z].status === "출근")
                            {
                                days[a] = 
                                    (day.getHours()>10? day.getHours():"0"+day.getHours()) + ":" + 
                                    (day.getMinutes()>10? day.getMinutes():"0"+day.getMinutes());
                            }
                            else
                            {
                                days[a] += " \r\n~\r\n " +
                                    (day.getHours()>10? day.getHours():"0"+day.getHours()) + ":" + 
                                    (day.getMinutes()>10? day.getMinutes():"0"+day.getMinutes());
                            }
                        }
                    }
                }
                
            }

            for(var i=0; i<json.totalResult; i++)
            {
                days[1] = json.results[i].name;
                break;
            }
        }
    })
    .then(data => {
        var sheet = workbook.addWorksheet('My Sheet');
        var worksheet = workbook.getWorksheet(1);
        
        // worksheet.getCell('C3').value = new Date(1968, 5, 1);

        // worksheet.getCell('B1').value = "1일"

        // 제목
        worksheet.mergeCells('B1:H2');
        worksheet.getCell("H2").value = year + "년 "+ month +"월 근태 현황";
        worksheet.getCell('H2').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell('H2').font = {
            name: '맑은 고딕',
            family: 4,
            size: 16,
            bold: true
        };

        //   // 날짜
        //   worksheet.mergeCells('C3:G3');
        //   worksheet.getCell("F3").value = 
        //                             year + "년 " + month + "월 1일 ~ " + 
        //                             year + "년 " + month + "월 " + ( new Date( year, month, 0) ).getDate() + "일";
        //   worksheet.getCell('F3').alignment = { vertical: 'middle', horizontal: 'center' };
        //   worksheet.getCell('F3').font = {
        //       name: '맑은 고딕',
        //       family: 4,
        //       size: 10,
        //       bold: true
        //   };

        // 개인정보
        worksheet.getCell("A4").value = "회사 : ";
        worksheet.getCell('A4').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.mergeCells('B4:C4');
        worksheet.getCell("C4").value = "피플씨엔에스";
        worksheet.getCell('C4').alignment = { vertical: 'middle', horizontal: 'left' };

        worksheet.getCell("D4").value = "소속 : ";
        worksheet.getCell('D4').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.mergeCells('E4:F4');
        worksheet.getCell("F4").value = attiboard[0].companyname;
        worksheet.getCell('F4').alignment = { vertical: 'middle', horizontal: 'left' };

        worksheet.getCell("G4").value = "이름";
        worksheet.getCell('G4').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.mergeCells('H4:I4');
        worksheet.getCell("I4").value = attiboard[0].username;
        worksheet.getCell('I4').alignment = { vertical: 'middle', horizontal: 'left' };

        
        // 본문 제목
        worksheet.mergeCells('A5:B5');
        worksheet.getCell("B5").value = "일 자";
        worksheet.getCell('B5').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell('B5').border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };
        worksheet.mergeCells('C5:C5');
        worksheet.getCell("C5").value = "출근날인";
        worksheet.getCell('C5').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell('C5').border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };

        worksheet.mergeCells('D5:E5');
        worksheet.getCell("E5").value = "일 자";
        worksheet.getCell('E5').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell('E5').border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };
        worksheet.mergeCells('F5:F5');
        worksheet.getCell("F5").value = "출근날인";
        worksheet.getCell('F5').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell('F5').border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };

        worksheet.mergeCells('G5:H5');
        worksheet.getCell("H5").value = "일 자";
        worksheet.getCell('H5').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell('H5').border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };
        worksheet.mergeCells('I5:I5');
        worksheet.getCell("I5").value = "출근날인";
        worksheet.getCell('I5').alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell('I5').border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };

        // 본문내용 요일
        // 1열
        for(var i=0; i<11; i++)
        {
            // console.log("1열 본문내용");
            worksheet.mergeCells('A'+(i*3+6)+':B'+(i*3+8));
            worksheet.getCell("B"+(i*3+8)).value = month + "월 "+ (i+1) +"일(" + week[new Date('2018-02-' + (i+1)).getDay()] +")";
            worksheet.getCell("B"+(i*3+8)).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getCell("B"+(i*3+8)).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };

            worksheet.mergeCells('C'+(i*3+6)+':C'+(i*3+8));
            // worksheet.getCell("C"+(i*3+8)).value = checkToday(week, year, month, (i+1) < 10 ? "0"+(i+1) : (i+1));
            // worksheet.getCell("C"+(i*3+8)).value = week[new Date(year + "-" + month + "-" + (i+1)).getDay()] == "토" ? "휴일" : (week[new Date(year + "-" + month + "-" + (i+1)).getDay()] == "일" ? "휴일" : "")
            worksheet.getCell("C"+(i*3+8)).value = days[i+1];
            worksheet.getCell("C"+(i*3+8)).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getCell("C"+(i*3+8)).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };
            worksheet.getCell("C"+(i*3+8)).font = {
                name: '맑은 고딕',
                family: 4,
                size: 10,
                bold: true
            };
        }
        // 2열
        for(var i=0; i<11; i++)
        {
            // console.log("2열 본문내용");
            worksheet.mergeCells('D'+(i*3+6)+':E'+(i*3+8));
            worksheet.getCell("E"+(i*3+8)).value = month + "월 "+ (i+12) +"일(" + week[new Date('2018-02-' + (i+12)).getDay()] +")";
            worksheet.getCell("E"+(i*3+8)).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getCell("E"+(i*3+8)).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };

            worksheet.mergeCells('F'+(i*3+6)+':F'+(i*3+8));
            // worksheet.getCell("F"+(i*3+8)).value = checkToday(week, year, month, (i+12) < 10 ? "0"+(i+12) : (i+12));
            // worksheet.getCell("F"+(i*3+8)).value = week[new Date(year + "-" + month + "-" + (i+12)).getDay()] == "토" ? "휴일" : (week[new Date(year + "-" + month + "-" + (i+12)).getDay()] == "일" ? "휴일" : "");
            worksheet.getCell("F"+(i*3+8)).value = days[i+12];
            worksheet.getCell("F"+(i*3+8)).alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.getCell("F"+(i*3+8)).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };
            worksheet.getCell("F"+(i*3+8)).font = {
                name: '맑은 고딕',
                family: 4,
                size: 10,
                bold: true
            };
        }
        // 3열
        for(var i=0; i<11; i++)
        {
            // console.log("3열 본문내용");
            if((i<lastDay-22) )
            {
                worksheet.mergeCells('G'+(i*3+6)+':H'+(i*3+8));
                worksheet.getCell("H"+(i*3+8)).value = month + "월 "+ (i+23) +"일(" + week[new Date('2018-02-' + (i+23)).getDay()] +")";
                worksheet.getCell("H"+(i*3+8)).alignment = { vertical: 'middle', horizontal: 'center' };
                worksheet.getCell("H"+(i*3+8)).border = {
                    top: {style:'thin'},
                    left: {style:'thin'},
                    bottom: {style:'thin'},
                    right: {style:'thin'}
                };

                // console.log(days[i*3+6]);
                worksheet.mergeCells('I'+(i*3+6)+':I'+(i*3+8));
                // worksheet.getCell("I"+(i*3+8)).value = checkToday(week, year, month, (i+23) < 10 ? "0"+(i+23) : (i+23));
                // worksheet.getCell("I"+(i*3+8)).value = days[i*3+6];
                // worksheet.getCell("I"+(i*3+8)).value = week[new Date(year + "-" + month + "-" + (i+23)).getDay()] == "토" ? "휴일" : (week[new Date(year + "-" + month + "-" + (i+23)).getDay()] == "일" ? "휴일" : "");
                worksheet.getCell("I"+(i*3+8)).value = days[i+23];
                worksheet.getCell("I"+(i*3+8)).alignment = { vertical: 'middle', horizontal: 'center' };
                worksheet.getCell("I"+(i*3+8)).border = {
                    top: {style:'thin'},
                    left: {style:'thin'},
                    bottom: {style:'thin'},
                    right: {style:'thin'}
                };
                worksheet.getCell("I"+(i*3+8)).font = {
                    name: '맑은 고딕',
                    family: 4,
                    size: 10,
                    bold: true
                };
            }
            else
            {
                worksheet.mergeCells('G'+(i*3+6)+':I'+(i*3+5+((11-i)*3)));
                worksheet.getCell("I"+(i*3+8)).value = "총 " + lastDay + "일";
                worksheet.getCell("I"+(i*3+8)).alignment = { vertical: 'middle', horizontal: 'center' };
                worksheet.getCell("I"+(i*3+8)).border = {
                    top: {style:'thin'},
                    left: {style:'thin'},
                    bottom: {style:'thin'},
                    right: {style:'thin'}
                };
                break;
            }
        }

        // 기간
        worksheet.mergeCells('A39:I39');
        worksheet.getCell("I39").value = "기간 : " + year + "년 " + month + "월 01일 ~ " + year + "년 " + month + "월 " + lastDay + "일"  ;
        worksheet.getCell("I39").alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell("I39").border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };

        // 서명
        worksheet.getCell("F40").value = "관리자";
        worksheet.getCell("F40").alignment = { vertical: 'middle', horizontal: 'center' };
        worksheet.getCell("F40").font = {
            family: 2,
            size: 16
        };
        worksheet.getCell("I40").value = "(인)";
        worksheet.getCell("I40").alignment = { vertical: 'middle', horizontal: 'left' };
        worksheet.getCell("I40").font = {
            family: 2,
            size: 16
        };

        try
        { 
            if(path.join(__dirname, "..", "public", "temp") !== '')
                {
                    lastfile = path.join(__dirname, "..", "public", "temp");
                    fs.mkdirSync(lastfile);
                    console.log("Make folder = " + lastfile);
                }
            else
                console.log('Folder is empty.');
        }
        catch(e)
        { 
            if ( e.code != 'EEXIST' ) 
                throw e; // 존재할경우 패스처리함. 
        }
             
        workbook.xlsx.writeFile(path.join(__dirname, "..", "public", "temp", filename))
        .then(data => {
        res.download(path.join(__dirname, "..", "public", "temp", filename));
        });
        // callback(null);
    }

      // return res.json(attiboard);
    )//.then(() => res.status(200).send());

    });
};


// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.attiBoard.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const userindex= req.body.userindex || '';
  const userid= req.body.userid || '';
  const writer= req.body.writer || '';
  const status= req.body.status || '';
  const memo= req.body.memo || '';

  if (!userid) {
    return res.status(400).json({error: 'Incorrect id'});
  }

    models.attiBoard.create({
      userindex: userindex,
      userid: userid,
      writer: writer,
      status: status,
      memo: memo
    },
  ).then((data) => res.status(201).json(data))
};

// 업데이트
exports.update = (req, res) => {
  const id = req.params.id;
  const body = req.body;

  // const userid = body.userid || '0';
  const online = body.online || '0';

  // console.log(online);

  models.User.update(
    {
      online: online
    }, 
    {
      where: 
      {
        userid:id
      }
    })
    .then((data) => res.status(200).json(data))
    .then(() => req.session.passport.user.online = online)
  }

  