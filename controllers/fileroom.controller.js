const models = require('../models/models');
const moment = require('moment');

/* 태이블 정보
    title: Sequelize.STRING,
    content: Sequelize.STRING,
    writer: Sequelize.INTEGER
*/

// 전체 찾기
exports.index = (req, res) => {
    models.fileroom.findAll()
    .then(data => res.status(200).json(
      {
        'data' : data
      }
    ));
};
   
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.notiBoard.findOne({
      where: {
        id: id
      }
    }).then(data => {
      if (!data) {
        return res.status(404).json({error: 'No Notiboard'});
      }
  
      return res.json(data);
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.fileroom.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const userindex = req.body.userindex || '';
    const userid = req.body.userid || '';
    const writer = req.body.writer || '';
    const filename = req.body.filename || '';
    const filepath = req.body.filepath || '';
  
    models.fileroom.create({
      userindex: userindex,
      userid: userid,
      writer: writer,
      filename: filename,
      filepath: filepath
    }).then((data) => res.status(200).json(data))
};

// 업데이트
exports.update = (req, res) => {
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  const title = body.title || '';
  const content = body.content || '';

  models.notiBoard.update(
    {
      title: title,
      content: content
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((user) => res.status(200).json(user))
  }

  