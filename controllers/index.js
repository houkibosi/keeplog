const express = require('express');
const expressSession =  require('express-session');
const router = express.Router();
const auth = require('../auth/auth');

const fs = require('fs');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const mime = require('mime-types');
const path = require('path');

const iconvLite = require('iconv-lite');

const multer = require('multer');
const _storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/public/uploads');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})
const upload = multer({storage: _storage});

const models = require('../models/models');


// const upload = multer({
//     storage: multer.diskStorage({
//       destination: function (req, file, cb) {
//         cb(null, 'd:\\');
//       },
//       filename: function (req, file, cb) {
//         cb(null, file.originalname);
//       }
//     }),
//   });
// const  multiparty = require('multiparty');

const formidable = require('formidable');


// main Pages
// router.post('/', passport.authenticate('local',
//     function(req, res) {
//         successRedirect: '/index'
//     }
// ));
// router.route('/').post(passport.authenticate('local'),
//     function(req, res){
//         res.redirect('/' + req.user.username);
//     }
// );

router.get('/2', (req, res) => {
    res.render('index.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/', (req, res) => {
    res.render('index2.ejs', {
        title : 'Express',
        user: req.user
    });
});


router.get('/calculator', (req, res) => {
    res.render('calculator.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/login', (req, res) => {
    res.render('login.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/dashboard', isLoggedin, (req, res) => {
    res.render('dashboard.ejs', {
        title : 'Express',
        user: req.user
    });
});


// 공지사항
router.get('/notice', isLoggedin, (req, res) => {
    res.render('notice.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/notice-new', isLoggedin, (req, res) => {
    res.render('notice-new.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/notice-detail', isLoggedin, (req, res) => {
    res.render('notice-detail.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/notice-update', isLoggedin, (req, res) => {
    res.render('notice-update.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 업무보고
router.get('/report', isLoggedin, (req, res) => {
    res.render('report.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/report-new', isLoggedin, (req, res) => {
    res.render('report-new.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/report-detail', isLoggedin, (req, res) => {
    res.render('report-detail.ejs', {
        title : 'Express',
        user: req.user
    });
});
router.get('/report-update', isLoggedin, (req, res) => {
    res.render('report-update.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 근태 관리
router.get('/attitude', isLoggedin, (req, res) => {
    res.render('attitude.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 그룹 / 사용자 관리
router.get('/manageuser', isLoggedin, (req, res) => {
    res.render('manageuser.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 사용자 정보관리
router.get('/userinfo', isLoggedin, (req, res) => {
    res.render('userinfo.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 매뉴얼
router.get('/manual', isLoggedin, (req, res) => {
    // res.render('manual/Keeplog Manual.ejs', {
        res.render('manual/documentation.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 계산기1
router.get('/ersmSemi', (req, res) => {
        res.render('ersmSemi.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 매뉴얼
router.get('/ersmSemi2', (req, res) => {
        res.render('ersmSemi02.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 매뉴얼
router.get('/ersmSemi3', (req, res) => {
        res.render('ersmSemi03.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 결제서류
router.get('/payment', isLoggedin, (req, res) => {
        res.render('payment.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 서식 다운
router.get('/reference', isLoggedin, (req, res) => {
        res.render('reference.ejs', {
        title : 'Express',
        user: req.user
    });
});

// 자료실
router.get('/fileroom', isLoggedin, (req, res) => {
        res.render('fileroom.ejs', {
        title : 'Express',
        user: req.user
    });
});





// User API
const user = require('./user.controller');
router.get('/users', user.index);
router.get('/users/:companyid', user.show);
router.delete('/users/:id', user.destroy);
router.post('/users', user.create);
router.put('/users/:id', user.update);

// User API
const user2 = require('./user2.controller');
router.get('/users2', user2.index);
router.get('/users2/:userid', user2.show);
router.delete('/users2/:id', user2.destroy);
router.post('/users2', user2.create);
router.put('/users2/:id', user2.update);

// UserList Down API
const userlist = require('./userlist.controller');
router.get('/userlists', userlist.index);
router.get('/userlists/:companyid', userlist.show);
router.delete('/userlists/:id', userlist.destroy);
router.post('/userlists', userlist.create);
router.put('/userlists/:id', userlist.update);

// login API
const login = require('./login.controller');
//router.post('/login', passport.authenticate('local', {successRedirect:'/index', failureRedirect: '/'}));
router.post('/login', passport.authenticate('local-login', {
    successRedirect: '/dashboard',
    failureRedirect: '/login'
}));
router.put('/login/:id', login.update);
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

// Company API
const company = require('./company.controller');
router.get('/companys', company.index);
router.get('/companys/:id', company.show);
router.delete('/companys', company.destroy);
router.post('/companys', company.create);
router.put('/companys', company.update);

// Noti API
const notice = require('./notice.controller');
router.get('/notices', notice.index);
router.get('/notices/:id', notice.show);
router.delete('/notices/:id', notice.destroy);
router.post('/notices', notice.create);
router.put('/notices/:id', notice.update);

// Noti2 API
const notice2 = require('./notice2.controller');
router.get('/notices2', notice2.index);
router.get('/notices2/:companyid', notice2.show);
router.delete('/notices2/:id', notice2.destroy);
router.post('/notices2', notice2.create);
router.put('/notices2/:id', notice2.update);

// Noti Reply API
const notireply = require('./noticereply.controller');
router.get('/notireplys', notireply.index);
router.get('/notireplys/:boardid', notireply.show);
router.delete('/notireplys/:boardid', notireply.destroy);
router.post('/notireplys', notireply.create);
router.put('/notireplys/:boardid', notireply.update);

// Report API
const report = require('./report.controller');
router.get('/reports', report.index);
router.get('/reports/:id', report.show);
router.delete('/reports/:id', report.destroy);
router.post('/reports', report.create);
router.put('/reports/:id', report.update);

// ReportList Down API
const reportlist = require('./reportlist.controller');
router.get('/reportlists', reportlist.index);
router.get('/reportlists/:userid/:year/:month/:week', reportlist.show);
router.delete('/reportlists/:id', reportlist.destroy);
router.post('/reportlists', reportlist.create);
router.put('/reportlists/:id', reportlist.update);

// Payment API
const payment = require('./payment.controller');
router.get('/payments', payment.index);
router.get('/payments/:id', payment.show);
router.delete('/payments/:id', payment.destroy);
router.post('/payments', payment.create);
router.put('/payments/:id', payment.update);

router.get('/getYear', function(req, res){
    const userindex = req.body.userindex || '';
   
    if (!userindex) {
        return res.status(400).json({error: 'Incorrect userindex'});
    }
    

    models.reportBoard.findAll({

    where: {
        userindex: userindex
    }
    }).then(data => {
    if (!data) {
        return res.status(404).json({error: 'No data'});
    }

    return res.json(data);
    });
})

// Upload API
const uploadFile = require('./upload.controller');
router.get('/uploads', uploadFile.index);
router.get('/uploads/:id', uploadFile.show);
router.delete('/uploads/:id', uploadFile.destroy);
// router.post('/uploads', upload.single('userfile'), uploadFile.create);
router.put('/uploads/:id', uploadFile.update);


router.post('/uploads', (req, res) => {
    const form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, "..", "public", "uploads");
    var oldPath = '';
    var fileName = '';
    var lastfile = '';
    form.keepExtensions = true;

    
    form.on('error', function(err) {
        console.log(err);
        throw err;
    })

    form.on('field', function(field, value) {
        // form.uploadDir = value;
    })

    form.on('fileBegin', function(name, file) {      
        // file.path = form.uploadDir + "/" + file.name;
        file.path = path.join(form.uploadDir, file.name);
        console.log("name = " + file.name);
        console.error("Join path ->>> ", file.path);
        // path.normalize(file.path);
        oldPath = file.path;

        var fileName = file.name.split("\\");
        var nLength = fileName.length;
        fileName = fileName[nLength-1];
        console.log("fileName = " + fileName);
        res.json(fileName);
    })

    form.on('file', function(field, file) {
        console.log(file);
    })

    form.on('progress', function(bytesReceived, bytesExpected) {
        // console.log('progress');

        var percent = (bytesReceived / bytesExpected * 100) | 0;
        process.stdout.write('uploading : %' + percent + '\r');
    })

    form.on('end', function(req, res) {
        try
        { 
            console.log("try");
            if(form.uploadDir !== '')
                {
                    console.log("if in");
                    //lastfile = path.join(__dirname, form.uploadDir);
                    fs.mkdirSync(form.uploadDir);
                }
            else
                console.log('form.uploadDir is empty.');
        }
        catch(e)
        { 
            console.log("catch");
            if ( e.code != 'EEXIST' ) 
                throw e; // 존재할경우 패스처리함. 
        }
        console.log("renmae");
        console.log(oldPath);
        fs.rename(oldPath, oldPath, (error)=>{console.log(error)});
        console.log('lastfile + fileName = ' + path.join(form.uploadDir, fileName));
        console.log('form end: \n\n');
    });

    form.parse(req, function(err){
        console.log('form parse : \n\n');
    });
});

router.get('/download/:id', function(req, res){
    const filename = req.params.id;
    console.log(filename);
    filepath = path.join(__dirname, "..", "public", "uploads", filename);
    mimetype = mime.lookup('application/octet-stream');
    res.setHeader('Content-disposition', 'attachment; filename=' + getDownloadFilename(req, filename));
    res.setHeader('Content-type', mimetype);
    var filestream = fs.createReadStream(filepath);
    filestream.pipe(res);


    // res.download(filepath);
})

// 한글파일 다운 시
function getDownloadFilename(req, filename) {
    var header = req.headers['user-agent'];
 
    if (header.includes("MSIE") || header.includes("Trident")) { 
        return encodeURIComponent(filename).replace(/\\+/gi, "%20");
    } else if (header.includes("Chrome")) {
        return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), 'ISO-8859-1');
    } else if (header.includes("Opera")) {
        return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), 'ISO-8859-1');
    } else if (header.includes("Firefox")) {
        return iconvLite.decode(iconvLite.encode(filename, "UTF-8"), 'ISO-8859-1');
    }
 
    return filename;
}

// Report2 API
const report2 = require('./report2.controller');
router.get('/reports2/:userindex', report2.index);
router.get('/reports2/:userid/:year/:month/:week', report2.show);
router.delete('/reports2/:id', report2.destroy);
router.post('/reports2', report2.create);
router.put('/reports2/:id', report2.update);

// Report3 API
const report3 = require('./report3.controller');
router.get('/reports3', report3.index);
router.get('/reports3/:companyid/:temp', report3.show);
router.delete('/reports3/:id', report3.destroy);
router.post('/reports3', report3.create);
router.put('/reports3/:id', report3.update);

// Report Reply API
const reportreply = require('./reportreply.controller');
router.get('/reportreplys', reportreply.index);
router.get('/reportreplys/:boardid', reportreply.show);
router.delete('/reportreplys/:boardid', reportreply.destroy);
router.post('/reportreplys', reportreply.create);
router.put('/reportreplys/:boardid', reportreply.update);

// Attitude API
const atti = require('./atti.controller');
router.get('/attitudes', atti.index);
router.get('/attitudes/:id', atti.show);
router.delete('/attitudes/:id', atti.destroy);
router.post('/attitudes', atti.create);
router.put('/attitudes/:id', atti.update);

// Attitude2 API
const atti2 = require('./atti2.controller');
router.get('/attitudes2/:companyindex/:temp', atti2.index);
router.get('/attitudes2/:id', atti2.show);
router.delete('/attitudes2/:id', atti2.destroy);
router.post('/attitudes2', atti2.create);
router.put('/attitudes2/:id', atti2.update);

// AttitudeList API
const attilist = require('./attilist.controller');
router.get('/attilists', attilist.index);
router.get('/attilists/:id/:year/:month', attilist.show);
router.delete('/attilists/:id', attilist.destroy);
router.post('/attilists', attilist.create);
router.put('/attilists/:id', attilist.update);

// 서식 다운 API
const reference = require('./reference.controller');
router.get('/references', reference.index);
router.get('/references/:id/:year/:month', reference.show);
router.delete('/references/:id', reference.destroy);
router.post('/references', reference.create);
router.put('/references/:id', reference.update);

// 자료실
const fileroom = require('./fileroom.controller');
router.get('/filerooms', fileroom.index);
router.get('/filerooms/:id/:year/:month', fileroom.show);
router.delete('/filerooms/:id', fileroom.destroy);
router.post('/filerooms', fileroom.create);
router.put('/filerooms/:id', fileroom.update);



// dashboard API
const dashboard = require('./dashboard.controller');
router.get('/dashboards', function(req, res){
    let queryStr = "";
    queryStr += "(SELECT count(userid) as data FROM users)"; //전체
    queryStr += "union";
    queryStr += "(SELECT count(distinct writer) as data from attiboards where createdAt >= curdate() and time(createdAt) < \"09:00:00\" and status = '출근')"; // 출근
    queryStr += "union";
    queryStr += "(SELECT count(distinct writer) as data from attiboards where createdAt >= curdate() and time(createdAt) > \"09:00:00\" and status = '출근')"; // 지각

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => {
      return res.json(data);
    });
})


// 인증 관리
function isLoggedin (req, res, next) 
{
    // console.log(req.isAuthenticated());
    // console.log(req.user);
    if (req.isAuthenticated())
    {
        console.log("logged in!!");
        console.log(req);

        var queryStr = '';
        queryStr += 'select timestampdiff(HOUR, createdAt, now()) difftime from attiboards ';
        queryStr += 'where userid = "'+req.user.userid+'" and createdAt > curdate() ';
        queryStr += 'order by id DESC limit 1';

        models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
        .then(data => {
            console.log(data);
            if(!data[0])
            {
                console.log("data is null");
                
                var queryStr2 = '';
                queryStr2 += 'insert into attiboards(userindex, userid, writer, status, createdAt, updatedAt, companyid, companyname) ';
                queryStr2 += 'select a.id, a.userid, a.name, "출근", now(), now(), b.id, b.name ';
                queryStr2 += 'from users a, companies b ';
                queryStr2 += 'where a.companyid = b.id and a.userid = "'+req.user.userid+'"; ';
                // queryStr2 += 'values('+ req.user.id +', "'+req.user.userid+'", "'+req.user.name+'", "출근", now(), now(),); ';
                
                console.log(queryStr2);
                models.sequelize.query(queryStr2, { type: models.sequelize.QueryTypes.INSERT})
                .then(()=> {return next()});
            }
            else
            {
                console.log("data is not null");
                console.log("difftime = " + req.user.worktime);
                if(data[0].difftime > data[0].worktime)
                {
                    console.log("difftime 1 over!!");

                    var queryStr2 = '';
                    queryStr2 += 'insert into attiboards(userindex, userid, writer, status, createdAt, updatedAt) ';
                    queryStr2 += 'values('+ req.user.id +', "'+req.user.userid+'", "'+req.user.name+'", "퇴근", now(), now()); ';
                    
                    models.sequelize.query(queryStr2, { type: models.sequelize.QueryTypes.INSERT})
                    .then(()=> {return next()});
                }
                else
                {
                    return next();
                }
            }
        })

        // return next();
    } 
    else 
    {
        res.redirect('/login');
    }
}


// export
module.exports = router;