const models = require('../models/models');
const moment = require('moment');

/* 태이블 정보
    title: Sequelize.STRING,
    content: Sequelize.STRING,
    writer: Sequelize.INTEGER
*/

// 전체 찾기
exports.index = (req, res) => {
    models.notiBoard.findAll({
      order: [['createdAt', 'DESC']]
    },
  ).then(data => res.status(200).json(
      {
        'data' : data
      }
    ));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const companyid = parseInt(req.params.companyid, 10);
    if (!companyid) {
      return res.status(400).json({error: 'Incorrect companyid'});
    }
  
    models.notiBoard.findAll({
      where: {
        companyid: companyid
      }
    }).then(data => {
      if (!data) {
        return res.status(404).json({error: 'No Notiboard'});
      }
  
      return res.status(200).json(
        {
          'data' : data
        });
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.notiBoard.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const title = req.body.title || '';
    const content = req.body.content || '';
    const writer = req.body.writer || '';
    const companyid = req.body.companyid || '';
  
    models.notiBoard.create({
      title: title,
      content: content,
      writer: writer,
      companyid: companyid 
    }).then((data) => res.status(200).json(data))
};

// 업데이트
exports.update = (req, res) => {
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  const title = body.title || '';
  const content = body.content || '';

  models.notiBoard.update(
    {
      title: title,
      content: content
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((user) => res.status(200).json(user))
  }

  