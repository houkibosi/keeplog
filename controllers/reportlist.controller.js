const models = require('../models/models');
var Excel = require('exceljs');
const fs = require('fs');
const mime = require('mime-types');
const path = require('path');

/* 테이블 정보
    companyid: Sequelize.INTEGER,
    team: Sequelize.STRING,
    userid: Sequelize.STRING,
    email: Sequelize.STRING,
    name: Sequelize.STRING,
    phone: Sequelize.STRING,
    password: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.User.findAll()
        .then(data => res.json(
          {
            'data' : data
          }
        ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const userindex = parseInt(req.params.userid, 10);
  const year = parseInt(req.params.year, 10);
  const month = parseInt(req.params.month, 10);
  const week = parseInt(req.params.week, 10);

  if (!year) {
    return res.status(400).json({error: 'Incorrect year'});
  }
  if (!month) {
    return res.status(400).json({error: 'Incorrect month'});
  }
  if (!week) {
    return res.status(400).json({error: 'Incorrect week'});
  }

    // models.reportBoard.findOne({
    //   where: {
    //     userindex: userindex,
    //     year: year,
    //     month: month,
    //     week: week
    //   }
    // })

    var queryStr = '';
    queryStr += 'SELECT report.*, comp.name as compname ';
    queryStr += 'FROM reportboards report, users users, companies comp ';
    queryStr += 'where report.userindex = users.id and users.companyid = comp.id and ';
    queryStr += 'report.userindex = "'+userindex+'" and report.year = "'+year+'" and report.month = "'+month+'" and report.week = "'+week+'";';

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => {
    if (!data) {
      return res.status(404).json({error: 'No data'});
    }

    console.log("data");
    console.log(data);
    var workbook = new Excel.Workbook();
    workbook.creator = 'Keeplog';
    workbook.lastModifiedBy = 'Keeplog';
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.lastPrinted = new Date();

    // var filename = "월간업무보고_" + data.writer + (workbook.modified.getMonth()+1) + "_" + workbook.modified.getDate() + ".xlsx";
    var filename = data[0].compname + "_" + data[0].writer + "_" + data[0].title + ".xlsx";
    var savepath = path.join(__dirname, "..", "public", "temp", filename);
    var sheet = workbook.addWorksheet('My Sheet');
    var worksheet = workbook.getWorksheet(1);

    // 제목
    worksheet.mergeCells('B3:H4');
    worksheet.getCell("G4").value = data[0].title;
    worksheet.getCell('G4').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('G4').font = {
        name: '맑은 고딕',
        family: 4,
        size: 20,
        bold: true
    };

    // 회사
    worksheet.getCell("A6").value = "회사";
    worksheet.getCell('A6').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.mergeCells('B6:C6');
    worksheet.getCell("C6").value = "피플씨엔에스";
    worksheet.getCell('C6').alignment = { vertical: 'middle', horizontal: 'center' };

    // 소속
    worksheet.getCell("D6").value = "소속";
    worksheet.getCell('D6').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.mergeCells('E6:F6');
    worksheet.getCell("F6").value = data[0].compname;
    worksheet.getCell('F6').alignment = { vertical: 'middle', horizontal: 'center' };

    // 이름
    worksheet.getCell("G6").value = "이름";
    worksheet.getCell('G6').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.mergeCells('H6:I6');
    worksheet.getCell("I6").value = data[0].writer;
    worksheet.getCell('I6').alignment = { vertical: 'middle', horizontal: 'center' };

    // 월
    worksheet.mergeCells('A7:B12');
    worksheet.getCell("B12").value = "월";
    worksheet.getCell('B12').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B12').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };
    worksheet.getCell('B12').font = {
      name: '맑은 고딕',
      family: 4,
      size: 20
    };  

    worksheet.mergeCells('C7:I12');
      var textMon = data[0].thismon.replace(/\n/g, '\r\n');//.replace(/\n/gi,"<br style='mso-data-placement:same-cell;'>");
      worksheet.getCell("I12").value = data[0].thismon;
      worksheet.getCell('I12').alignment = { vertical: 'top', horizontal: 'left' };
      worksheet.getCell('I12').border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    };

    // 화
    worksheet.mergeCells('A13:B18');
    worksheet.getCell("B18").value = "화";
    worksheet.getCell('B18').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B18').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };
    worksheet.getCell('B18').font = {
      name: '맑은 고딕',
      family: 4,
      size: 20
    };  

    worksheet.mergeCells('C13:I18');
      var texttue = data[0].thistue;//.replace(/\n/gi,"<br style='mso-data-placement:same-cell;'>");
      worksheet.getCell("I18").value = texttue;
      worksheet.getCell('I18').alignment = { vertical: 'top', horizontal: 'left' };
      worksheet.getCell('I18').border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    };

    // 수
    worksheet.mergeCells('A19:B24');
    worksheet.getCell("B24").value = "수";
    worksheet.getCell('B24').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B24').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };
    worksheet.getCell('B24').font = {
      name: '맑은 고딕',
      family: 4,
      size: 20
    };  

    worksheet.mergeCells('C19:I24');
      var textWed = data[0].thiswed;//.replace(/\n/gi,"<br style='mso-data-placement:same-cell;'>");
      worksheet.getCell("I24").value = textWed;
      worksheet.getCell('I24').alignment = { vertical: 'top', horizontal: 'left' };
      worksheet.getCell('I24').border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    };

    // 목
    worksheet.mergeCells('A25:B30');
    worksheet.getCell("B30").value = "목";
    worksheet.getCell('B30').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B30').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };
    worksheet.getCell('B30').font = {
      name: '맑은 고딕',
      family: 4,
      size: 20
    };  

    worksheet.mergeCells('C25:I30');
    var textThu = data[0].thisthu;//.replace(/\n/gi,"<br style='mso-data-placement:same-cell;'>");
      worksheet.getCell("I30").value = textThu;
      worksheet.getCell('I30').alignment = { vertical: 'top', horizontal: 'left' };
      worksheet.getCell('I30').border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    };

    // 금
    worksheet.mergeCells('A31:B36');
    worksheet.getCell("B36").value = "금";
    worksheet.getCell('B36').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B36').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };
    worksheet.getCell('B36').font = {
      name: '맑은 고딕',
      family: 4,
      size: 20
    };  

    worksheet.mergeCells('C31:I36');
      var textFri = data[0].thisfri;//.replace(/\n/gi,"<br style='mso-data-placement:same-cell;'>");
      worksheet.getCell("I36").value = textFri;
      worksheet.getCell('I36').alignment = { vertical: 'top', horizontal: 'left' };
      worksheet.getCell('I36').border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    };

    // 비고
    worksheet.mergeCells('A37:B42');
    worksheet.getCell("B42").value = "비고";
    worksheet.getCell('B42').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('B42').border = {
      top: {style:'thin'},
      left: {style:'thin'},
      bottom: {style:'thin'},
      right: {style:'thin'}
    };
    worksheet.getCell('B42').font = {
      name: '맑은 고딕',
      family: 4,
      size: 20
    };  

    worksheet.mergeCells('C37:I42');
      var textEtc = data[0].thisetc;//.replace(/\n/gi,"<br style='mso-data-placement:same-cell;'>");
      worksheet.getCell("I42").value = textEtc;
      worksheet.getCell('I42').alignment = { vertical: 'top', horizontal: 'left' };
      worksheet.getCell('I42').border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    };

    // 기간
    worksheet.mergeCells('C43:G43');
    worksheet.getCell("G43").value = "기간 : " + data[0].year + "년 " + data[0].month + "월" + data[0].startday + " ~ " + data[0].year + "년 " + data[0].month + "월 " + data[0].endday + "일"  ;
    worksheet.getCell("G43").alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell("G43").border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    };

    // // 작성자
    // worksheet.mergeCells('D38:F39');
    // worksheet.getCell("F39").value = "작성자";
    // worksheet.getCell('F39').alignment = { vertical: 'middle', horizontal: 'center' };
    // worksheet.getCell('F39').font = {
    //   name: '맑은 고딕',
    //   family: 4,
    //   size: 20
    // };  

    // // 인
    // worksheet.mergeCells('H38:I39');
    // worksheet.getCell("I39").value = "(인)";
    // worksheet.getCell('I39').alignment = { vertical: 'middle', horizontal: 'center' };
    // worksheet.getCell('I39').font = {
    //   name: '맑은 고딕',
    //   family: 4,
    //   size: 20
    // }; 

    try
    { 
        if(path.join(__dirname, "..", "public", "temp") !== '')
            {
                lastfile = path.join(__dirname, "..", "public", "temp");
                fs.mkdirSync(lastfile);
                console.log("Make folder = " + lastfile);
            }
        else
            console.log('Folder is empty.');
    }
    catch(e)
    { 
        if ( e.code != 'EEXIST' ) 
            throw e; // 존재할경우 패스처리함. 
    }

    console.log(savepath);
    workbook.xlsx.writeFile(savepath)
    .then(data => {
      res.download(path.join(__dirname, "..", "public", "temp", filename));
    })
  })
};


// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.User.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const name = req.body.name || '';
    const userid = req.body.userid || '';
    const email = req.body.email || '';
    const phone = req.body.phone || '';
    const password = req.body.password || '';
    const companyid = req.body.companyid || '';
    const team = req.body.team || '';
    const admin = req.body.admin || '';

    if (!name.length) {
      return res.status(400).json({error: 'Incorrenct name'});
    }
  
    models.User.create({
      name: name,
      userid: userid,
      email: email,
      phone: phone,
      password: password,
      companyid: companyid,
      team: team,
      admin: admin
    }).then((user) => res.status(201).json(user))
};

// 업데이트
exports.update = (req, res) => {
  console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  const name = body.name || '';
  const userid = body.userid || '';
  const email = body.email || '';
  const phone = body.phone || '';
  const password = body.password || '';
  const companyid = parseInt(body.companyid, 10);
  const team = body.team || '';  
  
  models.User.update(
    {
      name: name,
      userid: userid,
      email: email,
      phone: phone,
      password: password,
      companyid: companyid,
      team: team
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((user) => res.status(200).json(user))
      
  }

  