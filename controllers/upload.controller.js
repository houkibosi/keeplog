const models = require('../models/models');
const multiparty = require('multiparty');
// const upload = multer({
//   storage: multer.diskStorage({
//     destination: function (req, file, cb) {
//       cb(null, 'uploads/');
//     },
//     filename: function (req, file, cb) {
//       cb(null, file.originalname);
//     }
//   }),
// });

/* 태이블 정보
    writer: sequelize.INTEGER,
    title: Sequelize.STRING,
    thismon: sequelize.STRING,
    thistue: sequelize.STRING,
    thiswed: sequelize.STRING,
    thisthu: sequelize.STRING,
    thisfri: sequelize.STRING,
    thisetc: sequelize.STRING,
    nextmon: sequelize.STRING,
    nexttue: sequelize.STRING,
    nextwed: sequelize.STRING,
    nextthu: sequelize.STRING,
    nextfri: sequelize.STRING,
    nextetc: sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.reportBoard.findAll({
      order: [['createdAt', 'DESC']]
    }).then(data => res.json(
          {
            'data' : data
          }
        ));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.reportBoard.findOne({
      where: {
        id: id
      }
    }).then(reportboard => {
      if (!reportboard) {
        return res.status(404).json({error: 'No reportboard'});
      }
  
      return res.json(reportboard);
    });
};



// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.reportBoard.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  
    // if(req.file != null)
    // {
    //   console.log('req.file : ' + req.file);
    //   res.json('file uploaded');
    // }
    // else if(req.error != null)
    // {
    //   console.log('req.error : ' + req.error);
    //   res.json('error');
    // }
    // else
    // {
    //   console.log('Fail!!');
    //   res.json('Fail!!');
    // }
    

    // res.json('file uploaded'); // 파일과 예외 처리를 한 뒤 브라우저로 응답해준다.
};

  //   let form = new multiparty.Form({
//     autoFiles: ture, // autoFiles: 파일이 올라오면 자동으로 uploadDir에 저장을 할 것인지 true/false,
//     uploadDir: "D:\\써포트라인\\git\\public\\uploads", // uploadDir: 파일을를 업로드 할 절대경로,
//     maxFilesSize: 1024 * 1024 * 5 // 허용 파일 사이즈 최대치
// });

// form.parse(req, (error, fields, files) => {
//   // 파일 전송이 요청되면 이곳으로 온다.
//     // 에러와 필드 정보, 파일 객체가 넘어온다.

//   condole.log(req.file);
//   res.json('file uploaded'); // 파일과 예외 처리를 한 뒤 브라우저로 응답해준다.
// });
// };

// 업데이트
exports.update = (req, res) => {
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  const title= body.title || '';

  const thismon= body.thismon || '';
  const thistue= body.thistue || '';
  const thiswed= body.thiswed || '';
  const thisthu= body.thisthu || '';
  const thisfri= body.thisfri || '';
  const thisetc= body.thisetc || '';
  
  const nextmon= body.nextmon || '';
  const nexttue= body.nexttue || '';
  const nextwed= body.nextwed || '';
  const nextthu= body.nextthu || '';
  const nextfri= body.nextfri || '';
  const nextetc= body.nextetc || '';

  models.reportBoard.update(
    {
      title: title,

      thismon: thismon,
      thistue: thistue,
      thiswed: thiswed,
      thisthu: thisthu,
      thisfri: thisfri,
      thisetc: thisetc,

      nextmon: nextmon,
      nexttue: nexttue,
      nextwed: nextwed,
      nextthu: nextthu,
      nextfri: nextfri,
      nextetc: nextetc
    }, 
    {
      where: 
      {
        id:id
      }
    }).then((data) => res.status(200).json(data))
  }

  