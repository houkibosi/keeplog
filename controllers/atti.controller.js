const models = require('../models/models');

/* 태이블 정보
    writer: sequelize.INTEGER,
    status: Sequelize.INTEGER,
    memo: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.attiBoard.findAll()
        .then(data => res.json(
          {
            'data' : data
          }
        ));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.attiBoard.findOne({
      where: {
        id: id
      }
    }).then(attiboard => {
      if (!attiboard) {
        return res.status(404).json({error: 'No attiboard'});
      }
  
      return res.json(attiboard);
    });
};


// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.attiBoard.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const userid= req.body.userid || '';
  const userindex= req.body.userindex || '';  
  const writer= req.body.writer || '';
  const status= req.body.status || '';
  const memo= req.body.memo || '';

  if (!userid) {
    return res.status(400).json({error: 'Incorrect id'});
  }

  // var queryStr = '';
  // queryStr += 'select timestampdiff(HOUR, now(), createdAt) from attiboards ';
  // queryStr += 'where userid = "admin" and createdAt > curdate() ';
  // queryStr += 'order by id DESC limit 1';
  
  var queryStr2 = '';
  queryStr2 += 'insert into attiboards (userindex, userid, writer, status, createdAt, companyid, companyname) ';
  queryStr2 + 'SELECT a.id, a.userid, a.name, ';
  queryStr2 + 'if(strcmp(b.status, "출근") = 0, "퇴근", "출근"), now(), a.companyid, a.companyname  ';
  queryStr2 + 'FROM users a right outer join  attiboards b ';
  queryStr2 + 'on a.userid = b.userid ';
  queryStr2 + 'where a.userid = "'+userid+'" and b.createdAt > curdate() ';
  queryStr2 + 'order by b.id DESC limit 1';

  models.sequelize.query(queryStr)
  .then((data) => res.status(201).json(data))
    // models.attiBoard.create({
    //   userindex: userindex,
    //   userid: userid,
    //   writer: writer,
    //   status: status,
    //   memo: memo
    // },
  // )
    
};

// 업데이트
exports.update = (req, res) => {
  const id = req.params.id;
  const body = req.body;

  // const userid = body.userid || '0';
  const online = body.online || '0';

  // console.log(online);

  models.User.update(
    {
      online: online
    }, 
    {
      where: 
      {
        userid:id
      }
    })
    .then((data) => res.status(200).json(data))
    .then(() => req.session.passport.user.online = online)
  }

  