const models = require('../models/models');

/* 태이블 정보
    name: Sequelize.STRING
*/

// 전체 찾기
exports.index = (req, res) => {
    models.Company.findAll()
        .then(companys => res.status(200).json(companys));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.Company.findAll({
      where: {
        id: id
      }
    }).then(companys => {
      if (!companys) {
        return res.status(404).json({error: 'No Companys'});
      }
  
      return res.status(200).json(companys);
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.body.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.Company.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const name = req.body.name || '';
    if (!name.length) {
      return res.status(400).json({error: 'Incorrenct name'});
    }
  
    models.Company.create({
      name: name
    }).then((companys) => res.status(201).json(companys))
};

// 업데이트
exports.update = (req, res) => {
  const id = req.body.id;
  const name = req.body.name;

  // console.log("update id = " + id + ', name = ' + name);

  models.Company.update(
    {
      name: name
    }, 
    {
      where: 
      {
        id:id
      }
    })
    .then((data) => res.status(200).json(data))
  }

  