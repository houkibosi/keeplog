const models = require('../models/models');

/* 태이블 정보
    title: Sequelize.STRING,
    content: Sequelize.STRING,
    writer: Sequelize.INTEGER,
    order: sequelize.INTEGER
*/

// 전체 찾기
exports.index = (req, res) => {
    models.reportReply.findAll()
        .then(reportreply => res.json(reportreply));
};
  
// 1개 찾기
exports.show = (req, res) => {
    const boardid = parseInt(req.params.boardid, 10);
    //if (!id) {
    //  return res.status(400).json({error: 'Incorrect id'});
    //}
  
    models.reportReply.findAll({
      where: {
        boardid: boardid
      }
    }).then(reportreply => {
      if (!reportreply) {
        return res.status(404).json({error: 'No reportreply'});
      }
  
      return res.json(reportreply);
    });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.reportReply.destroy({
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const boardid = req.body.boardid || '';
  const content = req.body.content || '';
  const writer = req.body.writer || '';
    //if (!name.length) {
    //  return res.status(400).json({error: 'Incorrenct name'});
    //}
  
    models.reportReply.create({
      boardid: boardid,
      content: content,
      writer: writer
    }).then((reportreply) => res.status(201).json(reportreply))
};

// 업데이트
exports.update = (req, res) => {
    res.send();
  }

  