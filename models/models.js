const Sequelize = require('sequelize');
const sequelize = new Sequelize(
    // cafe24
    'keeplog', 
    'keeplog', 
    'Korea@2018!!', 
    {
        host: '10.0.0.1',
        port: 3306,
        dialect: 'mysql',
        operatorsAliases: false
    }

    
    // Local
    // 'supportline', 
    // 'root', 
    // 'p@ssw0rd', 
    // {
    //     host: '127.0.0.1',
    //     port: 3306,
    //     dialect: 'mysql',
    //     timezone: "+09:00"
    // }
    
);


// 사용자 정보 테이블
const User = sequelize.define('user', {
    companyid: {
        type: Sequelize.INTEGER,
        field: 'companyid'
    },
    companyname: {
        type: Sequelize.STRING,
        field: 'companyname'
    },
    team: {
        type: Sequelize.STRING,
        field: 'team'
    },
    userid: {
        type: Sequelize.STRING,
        field: 'userid'
    },
    email: {
        type: Sequelize.STRING,
        field: 'email'
    },
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    phone: {
        type: Sequelize.STRING,
        field: 'phone'
    },
    password: {   
        type: Sequelize.STRING,
        field: 'password',
        defaultValue: 'password'
    },
    admin: {
        type: Sequelize.BOOLEAN,
        field: 'admin'
    },
    online: {
        type: Sequelize.STRING,
        field: 'online'
    },
    worktime: {
        type: Sequelize.INTEGER,
        field: 'worktime'
    },
    address: {
        type: Sequelize.STRING,
        field: 'address'
    },
    inday: {
        type: Sequelize.STRING,
        field: 'inday'
    },
    outday: {
        type: Sequelize.STRING,
        field: 'outday'
    },
    unuselevel: {
        type: Sequelize.INTEGER,
        field: 'unuselevel'
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});



// 회사 정보 테이블
const Company = sequelize.define('company', {
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

// 공지사항
const notiBoard = sequelize.define('notiboards', {
    title: {
        type: Sequelize.STRING,
        field: 'title',
        allowNull: false
    },
    content: {
        type: Sequelize.STRING,
        field: 'content',
        allowNull: false
    },
    filepath: {
        type: Sequelize.STRING,
        field: 'filepath',
        allowNull: false
    },
    companyid: Sequelize.INTEGER,
    writer: Sequelize.STRING,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

// 공지사항 댓글
const notiReply = sequelize.define('notireply', {
    boardid: {
        type: Sequelize.INTEGER,
        field: 'boardid'
    },
    content: {
        type: Sequelize.STRING,
        field: 'content',
        allowNull: false
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer',
        allowNull: false
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

// 업무보고
const reportBoard = sequelize.define('reportboard', {
    userindex : {
        type: Sequelize.INTEGER,
        field: 'userindex'
    },
    companyid : {
        type: Sequelize.INTEGER,
        field: 'companyid'
    },
    userid: {
        type: Sequelize.STRING,
        field: 'userid',
        allowNull: false
    },
    companyname: {
        type: Sequelize.STRING,
        field: 'companyname',
        allowNull: false
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer',
        allowNull: false
    },
    filepath: {
        type: Sequelize.STRING,
        field: 'filepath',
        allowNull: false
    },
    year: {
        type: Sequelize.STRING,
        field: 'year',
        allowNull: false
    },
    month: {
        type: Sequelize.STRING,
        field: 'month',
        allowNull: false
    },
    week: {
        type: Sequelize.STRING,
        field: 'week',
        allowNull: false
    },
    startday: {
        type: Sequelize.STRING,
        field: 'startday',
        allowNull: false
    },
    endday: {
        type: Sequelize.STRING,
        field: 'endday',
        allowNull: false
    },
    title: {
        type: Sequelize.STRING,
        field: 'title',
        allowNull: false
    },
    thismon: {
        type: Sequelize.STRING,
        field: 'thismon',
        allowNull: false
    },
    thistue: {
        type: Sequelize.STRING,
        field: 'thistue',
        allowNull: false
    },
    thiswed: {
        type: Sequelize.STRING,
        field: 'thiswed',
        allowNull: false
    },
    thisthu: {
        type: Sequelize.STRING,
        field: 'thisthu',
        allowNull: false
    },
    thisfri: {
        type: Sequelize.STRING,
        field: 'thisfri',
        allowNull: false
    },
    thisetc: {
        type: Sequelize.STRING,
        field: 'thisetc',
        allowNull: false
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

// 업무보고 댓글
const reportReply = sequelize.define('reportreply', {
    boardid: {
        type: Sequelize.INTEGER,
        field: 'boardid'
    },
    content: {
        type: Sequelize.STRING,
        field: 'content'
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer',
        allowNull: false
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

// 근무상태
const attiStatus = sequelize.define('attistatus', {
    name: {
        type: Sequelize.STRING,
        field: 'name'
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

// 근태관리
const attiBoard = sequelize.define('attiboard', {
    userindex : {
        type: Sequelize.INTEGER,
        field: 'userindex'
    },
    companyid : {
        type: Sequelize.INTEGER,
        field: 'companyid'
    },
    userid : {
        type: Sequelize.STRING,
        field: 'userid'
    },
    companyname : {
        type: Sequelize.STRING,
        field: 'companyname'
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer',
        allowNull: false
    },
    status: {
        type: Sequelize.STRING,
        field: 'status',
        allowNull: false
    },
    memo: {
        type: Sequelize.STRING,
        field: 'memo'
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

// 서식다운
const reference = sequelize.define('reference', {
    userindex : {
        type: Sequelize.INTEGER,
        field: 'userindex'
    },
    userid: {
        type: Sequelize.STRING,
        field: 'userid',
        allowNull: false
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer',
        allowNull: false
    },
    filename: {
        type: Sequelize.STRING,
        field: 'filename',
        allowNull: false
    },
    filepath: {
        type: Sequelize.STRING,
        field: 'filepath',
        allowNull: false
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

// 자료실
const fileroom = sequelize.define('fileroom', {
    userindex : {
        type: Sequelize.INTEGER,
        field: 'userindex'
    },
    userid: {
        type: Sequelize.STRING,
        field: 'userid',
        allowNull: false
    },
    writer: {
        type: Sequelize.STRING,
        field: 'writer',
        allowNull: false
    },
    filename: {
        type: Sequelize.STRING,
        field: 'filename',
        allowNull: false
    },
    filepath: {
        type: Sequelize.STRING,
        field: 'filepath',
        allowNull: false
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
});

module.exports = {
    sequelize: sequelize,
    User: User,
    Company: Company,
    notiBoard: notiBoard,
    notiReply: notiReply,
    reportBoard: reportBoard,
    reportReply: reportReply,
    attiStatus: attiStatus,
    attiBoard: attiBoard,
    reference: reference,
    fileroom: fileroom
};