$(document).ready(function() {
	makebudam();
	
	$("#LEVYLACK_NUM").attr("readonly","readonly");
	$("#LEVYLACK_NUM").css('border', 'none');
	$("#LEVYLACK_NUM").css('background', 'transparent');
	$("#LEVYBAS_AMT").attr("readonly","readonly");
	$("#LEVYBAS_AMT").css('border', 'none');
	$("#LEVYBAS_AMT").css('background', 'transparent');
	$("#LEVYBASADD_NUM").attr("readonly","readonly");
	$("#LEVYBASADD_NUM").css('border', 'none');
	$("#LEVYBASADD_NUM").css('background', 'transparent');
	$("#LEVYBASICADD_AMT").attr("readonly","readonly");
	$("#LEVYBASICADD_AMT").css('border', 'none');
	$("#LEVYBASICADD_AMT").css('background', 'transparent');
	$("#LEVYBASADD2_NUM").attr("readonly","readonly");
	$("#LEVYBASADD2_NUM").css('border', 'none');
	$("#LEVYBASADD2_NUM").css('background', 'transparent');
	$("#LEVYBASICADD2_AMT").attr("readonly","readonly");
	$("#LEVYBASICADD2_AMT").css('border', 'none');
	$("#LEVYBASICADD2_AMT").css('background', 'transparent');
	$("#LEVYBASADD3_NUM").attr("readonly","readonly");
	$("#LEVYBASADD3_NUM").css('border', 'none');
	$("#LEVYBASADD3_NUM").css('background', 'transparent');
	$("#LEVYBASICADD3_AMT").attr("readonly","readonly");
	$("#LEVYBASICADD3_AMT").css('border', 'none');
	$("#LEVYBASICADD3_AMT").css('background', 'transparent');
	$("#LEVYNODIS_NUM").attr("readonly","readonly");
	$("#LEVYNODIS_NUM").css('border', 'none');
	$("#LEVYNODIS_NUM").css('background', 'transparent');
	$("#MINWAGE_AMT").attr("readonly","readonly");
	$("#MINWAGE_AMT").css('border', 'none');
	$("#MINWAGE_AMT").css('background', 'transparent');
	$("#LEVYCAL_AMT").attr("readonly","readonly");
	$("#LEVYCAL_AMT").css('border', 'none');
	$("#LEVYCAL_AMT").css('background', 'transparent');
	$("#LEVYCAL_AMT2").attr("readonly","readonly");
	$("#LEVYCAL_AMT2").css('border', 'none');
	$("#LEVYCAL_AMT2").css('background', 'transparent');
	$("#APPCAL_AMT").attr("readonly","readonly");
	$("#APPCAL_AMT").css('border', 'none');
	$("#APPCAL_AMT").css('background', 'transparent');
	$("#LEVYREDUC_AMT").attr("readonly","readonly");
	$("#LEVYREDUC_AMT").css('border', 'none');
	$("#LEVYREDUC_AMT").css('background', 'transparent');
	$("#LEVYAPP_AMT").attr("readonly","readonly");
	$("#LEVYAPP_AMT").css('border', 'none');
	$("#LEVYAPP_AMT").css('background', 'transparent');
	$("#LEVYREDUC_AMT2").attr("readonly","readonly");
	$("#LEVYREDUC_AMT2").css('border', 'none');
	$("#LEVYREDUC_AMT2").css('background', 'transparent');
	$("#LEVYQRT1_AMT").attr("readonly","readonly");
	$("#LEVYQRT1_AMT").css('border', 'none');
	$("#LEVYQRT1_AMT").css('background', 'transparent');
	$("#LEVYQRT2_AMT").attr("readonly","readonly");
	$("#LEVYQRT2_AMT").css('border', 'none');
	$("#LEVYQRT2_AMT").css('background', 'transparent');
	$("#LEVYQRT3_AMT").attr("readonly","readonly");
	$("#LEVYQRT3_AMT").css('border', 'none');
	$("#LEVYQRT3_AMT").css('background', 'transparent');
	$("#LEVYQRT4_AMT").attr("readonly","readonly");
	$("#LEVYQRT4_AMT").css('border', 'none');
	$("#LEVYQRT4_AMT").css('background', 'transparent');
	$("input[type=radio]").change(function(event){
		reduceSetting();
	});
	$("#REDUCTION_AMT").keyup(function(event){
		$(this).toPrice();
		reduceSetting();
	});
	$('input').focus(function(event) {
		$(this).select();
	});
	
	settingVal();
});

var report;
var jangdet;
function toInt(obj){
	return parseInt(obj == null ? 0 : obj);
}
function settingVal(){
	if(typeof $("#LEVYBAS_AMT").val() != "undefined") $("#LEVYBAS_AMT").toPrice();
	if(typeof $("#LEVYBASICADD_AMT").val() != "undefined") $("#LEVYBASICADD_AMT").toPrice();
	if(typeof $("#LEVYBASICADD2_AMT").val() != "undefined") $("#LEVYBASICADD2_AMT").toPrice();
	if(typeof $("#LEVYBASICADD3_AMT").val() != "undefined") $("#LEVYBASICADD3_AMT").toPrice();
	if(typeof $("#LEVYCAL_AMT").val() != "undefined") $("#LEVYCAL_AMT").toPrice();
	if(typeof $("#LEVYCAL_AMT2").val() != "undefined") $("#LEVYCAL_AMT2").toPrice();
	if(typeof $("#MINWAGE_AMT").val() != "undefined") $("#MINWAGE_AMT").toPrice();
	if(typeof $("#APPCAL_AMT").val() != "undefined") $("#APPCAL_AMT").toPrice();
	$("#REDUCTION_AMT").toPrice();
	$("#LEVYAPP_AMT").toPrice();
	$("#radio1").attr("checked","checked");
	reduceSetting();
}
function reduceSetting(){
	var oldamt = 0;
	if(typeof $("#LEVYCAL_AMT").val() != "undefined"){
		oldamt = $("#LEVYCAL_AMT").getOnlyNumeric();
	}
	var redu = Math.ceil(oldamt * 0.050) * 10;
	var reduamt = $("#REDUCTION_AMT").val().trim() == '' ? 0 : parseInt($("#REDUCTION_AMT").getOnlyNumeric());
	if(reduamt > redu){
		alert('연계고용 감면액은 납부하여야 할 고용부담금액의 50% 범위내에서 감면가능합니다.');
		$("#REDUCTION_AMT").val(0).toPrice();
		reduamt = 0;
	}
	oldamt = oldamt - reduamt;
	oldamt = oldamt < 0 ? 0 : oldamt;
	if(oldamt < 1000000){
		$("#radio1").attr("checked","checked");
		$("#LEVYQRT_YN").val("Y");
		$("#LEVYREDUC_AMT").val(0);
		$("#LEVYREDUC_AMT2").val(0);
		$("#LEVYQRT1_AMT").val(oldamt).toPrice();
		$("#LEVYQRT2_AMT").val(0);
		$("#LEVYQRT3_AMT").val(0);
		$("#LEVYQRT4_AMT").val(0);
		$("#LEVYAPP_AMT").val(oldamt).toPrice();
		$("#LEVYLAST_AMT").val(oldamt);
		$("#radio2").attr("disabled","disabled");
		$("#radio2").attr("style","background:#dfdfdf;");
	}else{
		$("#radio2").attr("disabled","");
		$("#radio2").attr("style","background:#ffffff;");
		if($("#radio1").attr("checked")){
			$("#LEVYQRT_YN").val("Y");
			var levy = Math.ceil(oldamt * 0.003) * 10;
			$("#LEVYREDUC_AMT").val(levy).toPrice();
			$("#LEVYREDUC_AMT2").val(levy).toPrice();
			$("#LEVYAPP_AMT").val(oldamt - levy).toPrice();
			$("#LEVYLAST_AMT").val(oldamt - levy);
			$("#LEVYQRT1_AMT").val(oldamt - levy).toPrice();
			$("#LEVYQRT2_AMT").val(0);
			$("#LEVYQRT3_AMT").val(0);
			$("#LEVYQRT4_AMT").val(0);
		}else{
			$("#LEVYQRT_YN").val("N");
			$("#LEVYREDUC_AMT").val(0);
			$("#LEVYREDUC_AMT2").val(0);
			$("#LEVYAPP_AMT").val(oldamt).toPrice();
			$("#LEVYLAST_AMT").val(oldamt);
			var bunhal = parseInt(oldamt / 4000) * 1000;
			$("#LEVYQRT1_AMT").val(bunhal + (oldamt % bunhal)).toPrice();
			$("#LEVYQRT2_AMT").val(bunhal).toPrice();
			$("#LEVYQRT3_AMT").val(bunhal).toPrice();
			$("#LEVYQRT4_AMT").val(bunhal).toPrice();
		}
	}
}

function makebudam(){
	var cnt = 0;
	var row = 0;
	var numcol = 0;
	var amtcol = 0;
	var numend = 0;
	var amtend = 0;
	var numchk = false;
	var amtchk = false;
	
	var num00 = "<label for='LEVYNODIS_NUM'>[ 미고용월 미달고용인원</label> X <label for='MINWAGE_AMT'>&nbsp;&nbsp;최저임금액&nbsp;&nbsp;&nbsp;]</label>";
	var num01 = "<label for='LEVYBASADD_NUM'>[ '1/4미달'의 월합계인원 </label> X <label for='LEVYBASICADD_AMT'>&nbsp;&nbsp;&nbsp;해당부담기초액&nbsp;&nbsp;]</label>";
	var num02 = "<label for='LEVYBASADD2_NUM'>[ '1/4~1/2미달'의 월합계인원 </label> X <label for='LEVYBASICADD2_AMT'>&nbsp;&nbsp;&nbsp;해당부담기초액&nbsp;&nbsp;]</label>";
	var num03 = "<label for='LEVYBASADD3_NUM'>[ '1/2~3/4미달'의 월합계인원 </label> X <label for='LEVYBASICADD3_AMT'>&nbsp;&nbsp;&nbsp;해당부담기초액&nbsp;&nbsp;]</label>";
	var num04 = "<label for='LEVYLACK_NUM'>[ '3/4이상'의 월합계인원 </label> X <label for='LEVYBAS_AMT'>&nbsp;&nbsp;&nbsp;부담기초액&nbsp;&nbsp;]</label>";
	var amt00 = "[ <input type='text' name='LEVYNODIS_NUM' id='LEVYNODIS_NUM' style='width:134px;text-align:center;' class='ip_text' value='"+$("#NUM00").val()+"'/> X <input type='text' name='MINWAGE_AMT' id='MINWAGE_AMT' style='width:89px;text-align:center;' class='ip_text' value='"+$("#AMT00").val()+"'/> ]";
	var amt01 = "[ <input type='text' name='LEVYBASADD_NUM' id='LEVYBASADD_NUM' style='width:143px;text-align:center;' class='ip_text' value='"+$("#NUM01").val()+"'/> X <input type='text' name='LEVYBASICADD_AMT' id='LEVYBASICADD_AMT' style='width:112px;text-align:center;' class='ip_text' value='"+$("#AMT01").val()+"'/> ]";
	var amt02 = "[ <input type='text' name='LEVYBASADD2_NUM' id='LEVYBASADD2_NUM' style='width:171px;text-align:center;' class='ip_text' value='"+$("#NUM02").val()+"'/> X <input type='text' name='LEVYBASICADD2_AMT' id='LEVYBASICADD2_AMT' style='width:114px;text-align:center;' class='ip_text' value='"+$("#AMT02").val()+"'/> ]";
	var amt03 = "[ <input type='text' name='LEVYBASADD3_NUM' id='LEVYBASADD3_NUM' style='width:171px;text-align:center;' class='ip_text' value='"+$("#NUM03").val()+"'/> X <input type='text' name='LEVYBASICADD3_AMT' id='LEVYBASICADD3_AMT' style='width:114px;text-align:center;' class='ip_text' value='"+$("#AMT03").val()+"'/> ]";
	var amt04 = "[ <input type='text' name='LEVYLACK_NUM' id='LEVYLACK_NUM' style='width:144px;text-align:center;' class='ip_text' value='"+$("#NUM04").val()+"' /> X <input type='text' name='LEVYBAS_AMT' id='LEVYBAS_AMT' style='width:86px;text-align:center;' class='ip_text' value='"+$("#AMT04").val()+"'/> ]";
	
	for(var i=0;i<5;i++){
		if($("#NUM0"+i).val() != 0){
			cnt++;
		}
	}
	
	row = Math.ceil(cnt/2)*2+2;
	if(cnt > 1 && cnt%2 == 0) row += 2;
	
	var sbHTML = getTableHeader();
	
	sbHTML.append("<tr>");
	sbHTML.append("<th rowspan='"+row+"' scope='row'>장애인 고용부담금<br /> 신고 납부액</th>");
	sbHTML.append("<th scope='col' colspan='3' style='text-align:left;'>");
	
	if(cnt > 0){
		
		for(var i=0;i<3;i++){
			if(numcol <= cnt){
				for(var j=numend;j<5;j++){
					if($("#NUM0"+j).val() > 0){
						if(numcol > 0){
							sbHTML.append("&nbsp;+ ");
						}
						sbHTML.append(eval("num0"+j));

						numcol++;
						numend = j+1;
						if(numcol > 0 && numcol%2 == 0) numchk = true;
					}

					if(numcol%2 == 1 && numcol == cnt){
						sbHTML.append("&nbsp;= &nbsp;<label for='LEVYCAL_AMT'>&nbsp;&nbsp;&nbsp;부담금액</label>");
						numcol++;
						numchk = true;
					}
					
					if(numcol > 0){
						if(numcol%2 == 0 && numchk){
							sbHTML.append("</th>");
							sbHTML.append("</tr>");
							sbHTML.append("<tr>");
							sbHTML.append("<td scope='row' colspan='3'>");
							
							numchk = false;
							
							break;
						}
					}
				}
				for(var j=amtend;j<5;j++){
					if($("#NUM0"+j).val() > 0){
						if(amtcol > 0){
							sbHTML.append("&nbsp;+ ");
						}
						sbHTML.append(eval("amt0"+j));
	
						amtcol++;
						amtend = j+1;
						if(amtcol > 0 && amtchk%2 == 0) amtchk = true;
					}
					
					if(amtcol%2 == 1 && amtcol == cnt){
						sbHTML.append("&nbsp;= <input type='text' name='LEVYCAL_AMT' id='LEVYCAL_AMT'style='width:90px;text-align:center;' class='ip_text' value='"+$("#BUDAM_AMT").val()+"'/>");
						amtcol++;
						amtchk = true;
					}
					
					if(amtcol > 0){
						if(amtcol%2 == 0 && amtchk){
							sbHTML.append("</td>");
							sbHTML.append("</tr>");
							sbHTML.append("<tr>");
							sbHTML.append("<th scope='col' colspan='3' style='text-align:left;'>");
							
							amtchk = false;
							
							break;
						}
					}
				}
			}
		}
		if(numcol%2 == 0 && numcol == cnt){
			sbHTML.append("&nbsp;= &nbsp;<label for='LEVYCAL_AMT'>&nbsp;&nbsp;&nbsp;부담금액</label>");
			sbHTML.append("</th>");
			sbHTML.append("</tr>");
			sbHTML.append("<tr>");
			sbHTML.append("<td scope='row' colspan='3'>");
			sbHTML.append("&nbsp;= <input type='text' name='LEVYCAL_AMT' id='LEVYCAL_AMT'style='width:90px;text-align:center;' class='ip_text' value='"+$("#BUDAM_AMT").val()+"'/>");
			sbHTML.append("</td>");
			sbHTML.append("</tr>");
			sbHTML.append("<tr>");
			sbHTML.append("<th scope='col' colspan='3' style='text-align:left;'>");
		}
	}
	
	sbHTML.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<label for='LEVYCAL_AMT2'>부담금액</label>]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-");
	sbHTML.append("&nbsp;&nbsp;&nbsp;&nbsp;[<label for='REDUCTION_AMT'>연계고용감면액</label>]&nbsp;&nbsp;&nbsp;&nbsp;-");
	sbHTML.append("&nbsp;&nbsp;[<label for='LEVYREDUC_AMT'>일시납공제액</label>]&nbsp;&nbsp;&nbsp;=");
	sbHTML.append("&nbsp;&nbsp;&nbsp;&nbsp;<label for='LEVYAPP_AMT'>실 납부금액</label> &nbsp;");
	sbHTML.append("</th>");
	sbHTML.append("</tr>");
	sbHTML.append("<tr>");
	sbHTML.append("<td scope='row' colspan='3'>");
	sbHTML.append("<input type='text' name='LEVYCAL_AMT2' id='LEVYCAL_AMT2' style='width:110px;text-align:center;' class='ip_text' value='"+$("#BUDAM_AMT").val()+"' /> - ");
	sbHTML.append("&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='REDUCTION_AMT' id='REDUCTION_AMT' style='width:106px;text-align:right;'class='ip_text' value='0'/>&nbsp;&nbsp;&nbsp;&nbsp; - ");
	sbHTML.append("<input type='text' name='LEVYREDUC_AMT' id='LEVYREDUC_AMT' style='width:110px;text-align:center;' class='ip_text'/>&nbsp;&nbsp;= ");
	sbHTML.append("<input type='text' name='LEVYAPP_AMT' id='LEVYAPP_AMT' style='width:110px;text-align:center;' class='ip_text'/>");
	sbHTML.append("</td>");
	sbHTML.append("</tr>");
	sbHTML.append("<tr>");
	sbHTML.append("<th scope='row'>납부 방법</th>");
	sbHTML.append("<td><input type='hidden' name='LEVYQRT_YN' id='LEVYQRT_YN'/><input type='radio' id='radio1' name='radio' /> <label for='radio1'> 일시납부 </label> <input type='radio' id='radio2' name='radio' /> <label for='radio2'> 분할납부 </label></td>");
	sbHTML.append("<th><label for='LEVYREDUC_AMT2'>전액일시납부공제액</label></th>");
	sbHTML.append("<td><input type='text' name='LEVYREDUC_AMT2' id='LEVYREDUC_AMT2' size='14' class='ip_text' style='text-align:center;'/> 일시납공제액(3%)</td>");
	sbHTML.append("</tr>");
	sbHTML.append("<tr>");
	sbHTML.append("<th scope='row'>참고사항</th>");
	sbHTML.append("<td colspan='3'>");
	sbHTML.append("<ol style='line-height:180%; color:#333;'>");
	sbHTML.append("<li>1. 미달고용인원 : 의무고용인원에서 장애인근로자 수를 뺀 인원</li>");
	sbHTML.append("<li>2. 부담기초액(2017년도 기준)<br />");
	sbHTML.append("<div style='width:100%; margin-left:15px;'>");
	sbHTML.append("<dl style='width:540px; margin:3px 0 5px 0;'>");
	sbHTML.append("<dt style='padding:3px 0 3px 0; background:#e2ebf4;border-left:1px solid #d2deeb; border-right:1px solid #d2deeb; border-bottom:1px solid #d2deeb; border-top:1px solid #d2deeb; text-align:center; font-weight:bold; color:#003a90;'>산정기준 및 부담금(미달인원 1인당)</dt>");
	sbHTML.append("<dd style='float:left; width:230px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:center;'>고용의무 인원의 3/4이상~4/4미만 고용</dd>");
	sbHTML.append("<dd style='float:left; width:294px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:left;'>&nbsp;부담기초액(월 812,000원)</dd>");
	sbHTML.append("<dd style='float:left; width:230px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:center;'>고용의무 인원의 1/2이상~3/4미만 고용</dd>");
	sbHTML.append("<dd style='float:left; width:294px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:left;'>&nbsp;부담기초액+부담기초액의 6% 가산(월 860,720원)</dd>");
	sbHTML.append("<dd style='float:left; width:230px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:center;'>고용의무 인원의 1/4이상~1/2미만 고용</dd>");
	sbHTML.append("<dd style='float:left; width:294px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:left;'>&nbsp;부담기초액+부담기초액의 20% 가산(월 974,400원)</dd>");
	sbHTML.append("<dd style='float:left; width:230px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:center;'>고용의무 인원의 1/4미만~1명이상 고용</dd>");
	sbHTML.append("<dd style='float:left; width:294px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:left;'>&nbsp;부담기초액+부담기초액의 40% 가산(월 1,136,800원)</dd>");
	sbHTML.append("<dd style='float:left; width:230px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:center;'>장애인을 한 명도 고용하지 않은 경우</dd>");
	sbHTML.append("<dd style='float:left; width:294px; margin:0; padding:3px; border: 1px solid #d2deeb; text-align:left;'>&nbsp;최저임금액(월 1,352,230원)</dd>");
	sbHTML.append("</dl>");
	sbHTML.append("</div>");
	/*
	sbHTML.append("<table width='70%' style='margin-left:15px;' cellspacing='0' summary='장애인 고용부담금 산정기준, 산정기준에 따른 해당 부담기초액에 관한 정보입력 테이블 입니다. '>");
	sbHTML.append("<caption>장애인 고용부담금 산정기준</caption>");
	sbHTML.append("<colgroup>");
	sbHTML.append("<col width='30%'/>");
	sbHTML.append("<col width='40%'/>");
	sbHTML.append("</colgroup>");
	sbHTML.append("<tbody>")
	sbHTML.append("<tr>");
	sbHTML.append("<th scope='col'>산정기준</th>");
	sbHTML.append("<th scope='col'>부담금(미달인원 1인당)</th>");
	sbHTML.append("</tr>");
	sbHTML.append("<tr>");
	sbHTML.append("<td style='text-align:center;'>고용의무 인원의 3/4이상~4/4미만 고용</td>");
	sbHTML.append("<td>&nbsp;부담기초액(월 757,000원)</td>");
	sbHTML.append("</tr>");
	sbHTML.append("<tr>");
	sbHTML.append("<td style='text-align:center;'>고용의무 인원의 1/2이상~3/4미만 고용</td>");
	sbHTML.append("<td>&nbsp;부담기초액+부담기초액의 10% 가산(월 832,700원)</td>");
	sbHTML.append("</tr>");
	sbHTML.append("<tr>");
	sbHTML.append("<td style='text-align:center;'>고용의무 인원의 1/4이상~1/2미만 고용</td>");
	sbHTML.append("<td>&nbsp;부담기초액+부담기초액의 20% 가산(월 908,400원)</td>");
	sbHTML.append("</tr>");
	sbHTML.append("<tr>");
	sbHTML.append("<td style='text-align:center;'>고용의무 인원의 1/4미만~1명이상 고용</td>");
	sbHTML.append("<td>&nbsp;부담기초액+부담기초액의 30% 가산(월 984,100원)</td>");
	sbHTML.append("</tr>");
	sbHTML.append("<tr>");
	sbHTML.append("<td style='text-align:center;'>장애인을 한 명도 고용하지 않은 경우</td>");
	sbHTML.append("<td>&nbsp;최저임금액(월 1,260,270원)</td>");
	sbHTML.append("</tr>");
	sbHTML.append("</tbody>")
	sbHTML.append("</table>");
	*/
	sbHTML.append("</li>");
	sbHTML.append("<li style='clear:both;'>3. 중증장애인근로자 : 월 임금지급 기초일 수 16일 이상이면서 월소정근로시간 60시간이상인 중증근로자는 2배수로 인정</li>");
	sbHTML.append("<li>4. 연계고용감면액 : 연계고용감면액은 해당이 있는 경우만 직접 입력</li>");
	sbHTML.append("</ol>");
	sbHTML.append("</td>");
	sbHTML.append("</tr>");
	
	sbHTML.append("</tbody>");
	sbHTML.append("</table>");
	$("#budam_grid").html(sbHTML.toString());
}

function getTableHeader(){
	var sbHTML = new StringBuffer();
	sbHTML.append("<table class='tb_base' cellspacing='0' summary='장애인 고용부담금 신고 납부액,납부방법, 전액일시납부공제액,부담금내역에 관한 정보입력 테이블 입니다. '>");
	sbHTML.append("<caption>장애인 고용부담금 내역확인</caption>");
	sbHTML.append("<colgroup>");
	sbHTML.append("<col width='16%'/>");
	sbHTML.append("<col width='32%'/>");
	sbHTML.append("<col width='20%'/>");
	sbHTML.append("<col width='32%'/>");
	sbHTML.append("</colgroup>");
	sbHTML.append("<tbody>");
	return sbHTML;
}