$(document).ready(function() {
	yyyy = $("#yyyy").val()*1;
	G_DimIndex = $("#company_type").val();
	$('#viewLoading').hide();
	$('input[type=text]').each(function(event) {
		$(this).css("ime-mode","disabled");
	});
	$('input[type=text]').keypress(function(event) {
		if (!event.which || !(event.which  > 47 && event.which  < 58 || event.which == 8)) { 
			event.preventDefault();
		}
	});
	$('input[type=text]').focus(function(event) {
		$(this).select();
	});
	$("input[type=text]").keyup(function(event){
		month = $(this).attr("id");
		var val1 = $(this).getOnlyNumeric()*1;
		$(this).val(val1);
		
		if(parseInt($("#"+month.substring(0,month.indexOf('_')+1)+"1").getOnlyNumeric()) < parseInt($("#"+month.substring(0,month.indexOf('_')+1)+"2").getOnlyNumeric())){
			alert('전체 근로자수 보다 적용 제외 근로자수가 많을 수 없습니다.');
			$("#"+month.substring(0,month.indexOf('_')+1)+"2").val($("#"+month.substring(0,month.indexOf('_')+1)+"1").getOnlyNumeric());
		}
		if((parseInt($("#"+month.substring(0,month.indexOf('_')+1)+"1").getOnlyNumeric())-parseInt($("#"+month.substring(0,month.indexOf('_')+1)+"2").getOnlyNumeric())) < (parseInt($("#"+month.substring(0,month.indexOf('_')+1)+"7_1").getOnlyNumeric())+parseInt($("#"+month.substring(0,month.indexOf('_')+1)+"8_1").getOnlyNumeric())+parseInt($("#"+month.substring(0,month.indexOf('_')+1)+"9_1").getOnlyNumeric())) ){
			alert('상시 근로자수 보다 장애인 근로자수가 많을 수 없습니다.');
			$("#"+month.substring(0,month.indexOf('_')+1)+"7_1").val(0);
			$("#"+month.substring(0,month.indexOf('_')+1)+"8_1").val(0);
			$("#"+month.substring(0,month.indexOf('_')+1)+"9_1").val(0);
		}
		
		WorkerCnt(month.substring(3,month.indexOf('_')));
		if(event.which == 9)
		$(this).select();
		if(event.keyCode == 13){
			if(month.substring(month.indexOf('_')+1,month.length) == '1'){
				if(month.substring(3,month.indexOf('_'))=='12'){
					$("#cal1_2").select();
				}else{
					$("#cal"+((month.substring(3,month.indexOf('_'))*1)+1)+"_1").select();
				}
			}else if(month.substring(month.indexOf('_')+1,month.length) == '2'){
				if(month.substring(3,month.indexOf('_'))=='12'){
					$("#cal1_7_1").select();
				}else{
					$("#cal"+((month.substring(3,month.indexOf('_'))*1)+1)+"_2").select();
				}
			}else if(month.substring(month.indexOf('_')+1,month.length) == '7_1'){
				if(month.substring(3,month.indexOf('_'))=='12'){
					$("#cal1_8_1").select();
				}else{
					$("#cal"+((month.substring(3,month.indexOf('_'))*1)+1)+"_7_1").select();
				}
			}else if(month.substring(month.indexOf('_')+1,month.length) == '8_1'){
				if(month.substring(3,month.indexOf('_'))=='12'){
					$("#cal1_9_1").select();
				}else{
					$("#cal"+((month.substring(3,month.indexOf('_'))*1)+1)+"_8_1").select();
				}
			}else if(month.substring(month.indexOf('_')+1,month.length) == '9_1'){
				if(month.substring(3,month.indexOf('_'))=='12'){
					$("#cal1_1").select();
				}else{
					$("#cal"+((month.substring(3,month.indexOf('_'))*1)+1)+"_9_1").select();
				}
			}
		}
	});
	makestantic();
});	

function showloding(){
	$('#viewLoading').fadeIn(500);
}

function hideloding(){
	$('#viewLoading').fadeOut(500);	
}

var cal;
var stantic;
var G_DimIndex;
var busiarr;
var dutyrate;
var bountrate;

function WorkerCnt(month){
	var total = $("#cal"+month+"_1").getOnlyNumeric();
	var dayin = $("#cal"+month+"_2").getOnlyNumeric();
	if(total == ''){
		total = 0;
	}
	if(dayin == ''){
		dayin = 0;
	}
	var s_jdnum = parseInt(total) - parseInt(dayin);
	var s_umnum = 0;
	var s_bsnum = 0;
	var midnum = $("#cal"+month+"_7_1").val();
	var mulnum = $("#cal"+month+"_8_1").val();
	var crinum = $("#cal"+month+"_9_1").val();
	if(midnum == ''){
		midnum = 0;
	}else{
		midnum = parseInt(midnum);
	}
	if(mulnum == ''){
		mulnum = 0;
	}else{
		mulnum = parseInt(mulnum);
	}
	if(crinum == ''){
		crinum = 0;
	}else{
		crinum = parseInt(crinum);
	}
	
	var disnum = midnum+mulnum+crinum;
	
	if(G_DimIndex == "02"){//공공기관
		dutyrate = stantic.PUBLICDUTY_RATE;
		bountrate = stantic.PUBLICDUTY_RATE;
	}else if(G_DimIndex == "03"){//국가지자체
		dutyrate = stantic.GOVERNMENT_RATE;
		bountrate = stantic.BOUNTYBASE_RATE;
	}else{//민간 및 기타 공공
		dutyrate = stantic.DUTYEMPLOYEE_RATE;
		bountrate = stantic.BOUNTYBASE_RATE;
	}
	s_umnum = Math.floor(s_jdnum*(dutyrate*100)/10000);
	s_bsnum = Math.ceil(s_jdnum*(bountrate*100)/10000);
	
	var mitot = 0;
	var mi1 = 0;
	var mi2 = 0;
	var mi3 = 0;
	var mi4 = 0;
	var minon = 0;
	var distot = disnum+mulnum;
	if(s_umnum > distot){
		if(distot < 1){
			minon = s_umnum;
			if(yyyy < 2011 || (yyyy == 2011 && parseInt(month) < 7)){
				mitot = s_umnum;
				minon = 0;
				mi2 = Math.floor(s_umnum/2);
			}
		}else if(Math.floor(s_umnum/4) > distot){
			mitot = s_umnum - distot;
			mi1 = s_umnum - distot;
			//mi2 = Math.floor(s_umnum/2) - Math.floor(s_umnum/4);
			//mi3 = Math.floor(s_umnum*3/4) - Math.floor(s_umnum/2);
			//mi4 = s_umnum - Math.floor(s_umnum*3/4);
		}else if(Math.floor(s_umnum/2) > distot){
			mitot = s_umnum - distot;
			mi2 = s_umnum - distot;
			//mi3 = Math.floor(s_umnum*3/4) - Math.floor(s_umnum/2);
			//mi4 = s_umnum - Math.floor(s_umnum*3/4);
		}else if(Math.floor(s_umnum*3/4) > distot){
			mitot = s_umnum - distot;
			mi3 = s_umnum - distot;
			//mi4 = s_umnum - Math.floor(s_umnum*3/4);
		}else{
			mitot = s_umnum - distot;
			mi4 = s_umnum - distot;
		}
		if(yyyy < 2013){
			mi3 = 0;
			mi4 = 0;
		}
	}
	
	$("#cal"+month+"_1").val(total).toPrice();
	$("#cal"+month+"_2").val(dayin).toPrice();
	$("#cal"+month+"_3").html(
			"<input type='hidden' id='cal"+month+"_3_1' name='cal"+month+"_3_1' value='"+s_jdnum+"'>"+
			formatCurrency(s_jdnum)
	);
	$("#cal"+month+"_4").html(
			"<input type='hidden' id='cal"+month+"_4_1' name='cal"+month+"_4_1' value='"+s_umnum+"'>"+
			formatCurrency(s_umnum)
	);
	/*
	$("#cal"+month+"_5").html(
			"<input type='hidden' id='cal"+month+"_5_1' name='cal"+month+"_5_1' value='"+s_bsnum+"'>"+
			s_bsnum
	);
	*/
	$("#cal"+month+"_6").html(
			"<input type='hidden' id='cal"+month+"_6_1' name='cal"+month+"_6_1' value='"+disnum+"'>"+
			formatCurrency(disnum)
	);
	$("#cal"+month+"_7_1").val(midnum).toPrice();
	$("#cal"+month+"_8_1").val(mulnum).toPrice();
	$("#cal"+month+"_9_1").val(crinum).toPrice();
	$("#cal"+month+"_10").html(
			"<input type='hidden' id='cal"+month+"_10_1' name='cal"+month+"_10_1' value='"+minon+"'>"+
			formatCurrency(minon)
	);
	$("#cal"+month+"_11").html(
			"<input type='hidden' id='cal"+month+"_11_1' name='cal"+month+"_11_1' value='"+mitot+"'>"+
			formatCurrency(mitot)
	);
	$("#cal"+month+"_12").html(
			"<input type='hidden' id='cal"+month+"_12_1' name='cal"+month+"_12_1' value='"+mi1+"'>"+
			formatCurrency(mi1)
	);
	$("#cal"+month+"_13").html(
			"<input type='hidden' id='cal"+month+"_13_1' name='cal"+month+"_13_1' value='"+mi2+"'>"+
			formatCurrency(mi2)
	);
	$("#cal"+month+"_14").html(
			"<input type='hidden' id='cal"+month+"_14_1' name='cal"+month+"_14_1' value='"+mi3+"'>"+
			formatCurrency(mi3)
	);
	$("#cal"+month+"_15").html(
			"<input type='hidden' id='cal"+month+"_15_1' name='cal"+month+"_15_1' value='"+mi4+"'>"+
			formatCurrency(mi4)
	);
	var hap = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	var cnt = 0;
	for(var i=1;i<13;i++){
		hap[1] += $("#cal"+i+"_1").val() == '' ? 0 : parseInt($("#cal"+i+"_1").getOnlyNumeric());
		hap[2] += $("#cal"+i+"_2").val() == '' ? 0 : parseInt($("#cal"+i+"_2").getOnlyNumeric());
		//if(parseInt($("#cal"+i+"_3_1").val()) > 0){
			//cnt++;
		//}
		for(var j=3;j<16;j++){
			hap[j] += parseInt($("#cal"+i+"_"+j+"_1").val());
		}
	}
	
	for(var i=1;i<16;i++){
		$("#hap"+i).html(formatCurrency(hap[i]));
		//if(cnt < 1){
			//$("#avg"+i).html("0");//*10)/10);
		//}else{
			//$("#avg"+i).html(Math.floor(hap[i]/cnt));//*10)/10);
		//}
	}
}

function makestantic(){
	var url = ctxRoot+ '/ersmSemiStantic.do';
	
	/**
	 * 액션 구분자
	 * cmd: 액션 분기하는 파라미터 값
	 */
	doProcess(url, "basic_yy="+$("#yyyy").val(), makestanticCallBack);
}
function makestanticCallBack(originalRequest){
	eval("var jsonObj = "+chgLinefeed(originalRequest.responseText));
    if(jsonObj.isError == undefined){
    	if(jsonObj.nullsession){
            location.href = "/cmmn/ersmSessionError.do";
        }else{
        	stantic = jsonObj.HashMap;
        	for(var i=1;i<13;i++)
        	WorkerCnt(i);
        }
    }
}
function next(){
	$("#top_pop").focus();
	showloding();
	$("#frm").attr("action",ctxRoot+ '/ersmSemi03.do');
	$("#frm").submit();
}