//문자열 교환
function replace( source,  target,  replace)
{
	var sourceData="";
	sourceData=source;

	if(sourceData==null) return "";
	if(target==null||target=="") return source;

	iTargetLen = target.length;

	sbfReplace="";
	i = 0;
	j = 0;

	while (j > -1)
	{
		j = sourceData.indexOf(target,i);
		if (j > -1)
		{
			
			sbfReplace+=sourceData.substring(i,j);
			sbfReplace+=replace;
			i = j + iTargetLen;
		}
	}
	sbfReplace+=sourceData.substring(i,sourceData.length);

	return sbfReplace;
}

//한글길이 체크
function getByteLength(s){
   if (s == null) return 0;

   var len = 0;

   for (var i = 0; i < s.length; i++) {
      var c = escape(s.charAt(i));
      if (c.length == 1) len ++;
      else if (c.indexOf("%u") != -1) len += 2;
      else if (c.indexOf("%") != -1) len += c.length / 3;
   }

   return len;
}

//한글 byte 수 계산
function calLength(val) {
	// 입력받은 문자열을 escape() 를 이용하여 변환한다.
    // 변환한 문자열 중 유니코드(한글 등)는 공통적으로 %uxxxx로 변환된다.
    var temp_estr = escape(val);
    var s_index   = 0;
    var e_index   = 0;
    var temp_str  = "";
    var cnt       = 0;

    // 문자열 중에서 유니코드를 찾아 제거하면서 갯수를 센다.
    while ((e_index = temp_estr.indexOf("%u", s_index)) >= 0)  // 제거할 문자열이 존재한다면
    {
      temp_str += temp_estr.substring(s_index, e_index);
      s_index = e_index + 6;
      cnt ++;
    }

    temp_str += temp_estr.substring(s_index);

    temp_str = unescape(temp_str);  // 원래 문자열로 바꾼다.

    // 유니코드는 2바이트 씩 계산하고 나머지는 1바이트씩 계산한다.
    return ((cnt * 2) + temp_str.length) + "";
}


// value의 최대 바이트 수를 체크
function maxLengthCheck(formValue, maxlength) {
    var temp;
    var bytes = 0;
    var len = formValue.length;

    for(ii=0; ii<len; ii++){
        temp = formValue.charAt(ii) ;

	//escape code의 길이가 4보다 크면 한글
	if(escape(temp).length > 4){
	    bytes += 2;
	}else{
	    bytes++;
	}
    }
    if(maxlength >= bytes){
	return true;
    } else {
	return false;
    }
}

// value의 최소 바이트 수를 체크
function minLengthCheck(formValue, minlength) {
    var temp;
    var bytes = 0;
    var len = formValue.length;

    for(ii=0; ii<len; ii++){
        temp = formValue.charAt(ii) ;

	//escape code의 길이가 4보다 크면 한글
	if(escape(temp).length > 4){
	    bytes += 2;
	}else{
	    bytes++;
	}
    }
    if(minlength <= bytes){
	return true;
    } else {
	return false;
    }

}

//소문자 --> 대문자 변경
function lower2Upper(obj) {
	obj.value=obj.value.toUpperCase();
}

//한글체크.한글입력금지
function hanCheck(obj) {
	for(i=0;i<obj.value.length;i++) {
		var a=obj.value.charCodeAt(i);
		if (a > 128) {
			alert('한글을 입력할 수 없습니다.');
			resetObj(obj);;
			return false;
		}
	}
}

// 특수문자,숫자 체크. 문자만 입력
function chrNumCheck(obj) {
	for(i=0;i<obj.value.length;i++) {
		var a=obj.value.charCodeAt(i);
		if ((a > 32 && a < 65) || (a > 90 && a < 97) || (a > 122 && a < 129)) {
			alert("특수문자 또는 숫자를 입력할 수 없습니다.");
			resetObj(obj);
			return false;
		}
	}
}

// 특수문자,한글 체크. 숫자,영어,'-'만 입력
function chrHanCheck(obj) {
	for(i=0;i<obj.value.length;i++) {
		var a=obj.value.charCodeAt(i);
		if ((a > 32 && a < 45) || (a > 45 && a < 48) ||(a > 57 && a < 65) || (a > 90 && a < 97) || (a > 122)) {
			alert("'-'을 제외한 특수문자 또는 한글을 입력할 수 없습니다.");
			resetObj(obj);
			return false;
		}
	}
}

// 특수문자,한글,영어 체크. 숫자,'-'만 입력
function chrHanEngCheck(obj) {
	for(i=0;i<obj.value.length;i++) {
		var a=obj.value.charCodeAt(i);
		if ((a > 32 && a < 45) || (a > 45 && a < 48) ||(a > 57)) {
			alert("'-'을 제외한 특수문자 또는 한글,영문자를 입력할 수 없습니다.");
			resetObj(obj);
			return false;
		}
	}
}

function formatDate(obj, formatType, seperator) {
	var formatString;
	var DatedValue = obj.value ;
	var fieldValue = DatedValue.replace(/\/|\-/g, "");

	if (DatedValue == "") return "";

	var vYear = fieldValue.substr(0,4);
	var vMonth = fieldValue.substr(4,2);
	var vDay = fieldValue.substr(6,2);
	
	if (!isCorrectDate(vYear, vMonth, vDay))
	{
		alert( "날짜 자료가 아닙니다.\n 다시 입력하십시오." );
		obj.value = "";
		obj.focus();
		return false;
	}

	if (formatType == null) formatType = 1

	switch ( formatType )
	{
		case 1:	//yyyymmdd
			var sYear = fieldValue.substr(0,4);
			var sMonth = fieldValue.substr(4,2);
			var sDay = fieldValue.substr(6,2);

			formatString = sYear + seperator + sMonth + seperator + sDay;
			break;
		case 2: //mmddyyyy
			var sMonth = fieldValue.substr(0,2);
			var sDay = fieldValue.substr(3,2);
			var sYear = fieldValue.substr(6,4);

			formatString = sMonth + seperator + sDay + seperator + sYear;
			break;
		case 3: //ddmmyyyy
			var sDay = fieldValue.substr(0,2);
			var sMonth = fieldValue.substr(3,2);
			var sYear = fieldValue.substr(6,4);

			formatString = sDay + seperator + sMonth + seperator + sYear;
			break;
		case 4: //yyyymm
			var sYear = fieldValue.substr(0,4);
			var sMonth = fieldValue.substr(4,2);
			var sYear = "01";

			formatString = sDay + seperator + sMonth + seperator + sYear;
			break;
		default:
			alert ( "지원하지 않는 포맷입니다.");
			return false;
	}
	
	obj.value = formatString;
	return obj.value;
}

function getDaysOfMonth(year,month) {
	if(month < 1 || month >12)
		return -1;

	if(month == 2){
		if((year% 4 == 0 && year % 100 !=0)|| year%400==0)
			return 29;
		else
			return 28;
	}else if(month==4||month==6||month==9||month==11){
		return 30;
	}else{
		return 31;
	}
}

/**
 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
 *
 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
 * @return 기간에 해당하는 일자
 * 2004.08.19 박세원 추가
 */
function calDateRange(val1, val2) {

	var start_dt = new Array(["", "", ""]);
	var end_dt = new Array(["", "", ""]);

	// 년도, 월, 일로 분리
	if (val1.toString().length == 8) {
		start_dt[0] = val1.toString().substring(0, 4);
		start_dt[1] = val1.toString().substring(4, 6);
		start_dt[2] = val1.toString().substring(6);
	} else if (val1.toString().length == 10) {
		start_dt[0] = val1.toString().substring(0, 4);
		start_dt[1] = val1.toString().substring(5, 7);
		start_dt[2] = val1.toString().substring(8);
	}

	if (val2.toString().length == 8) {
		end_dt[0] = val2.toString().substring(0, 4);
		end_dt[1] = val2.toString().substring(4, 6);
		end_dt[2] = val2.toString().substring(6);
	} else if (val2.toString().length == 10) {
		end_dt[0] = val2.toString().substring(0, 4);
		end_dt[1] = val2.toString().substring(5, 7);
		end_dt[2] = val2.toString().substring(8);
	}

	// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
	// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
	start_dt[1] = (Number(start_dt[1]) - 1) + "";
	end_dt[1] = (Number(end_dt[1]) - 1) + "";

	var from_dt = new Date(start_dt[0], start_dt[1], start_dt[2]);
	var to_dt = new Date(end_dt[0], end_dt[1], end_dt[2]);

	return (to_dt.getTime() - from_dt.getTime()) / 1000 / 60 / 60 / 24;
}

//금액 형식
function formatCurrency(num) {
	var numData="";
	var startRealNum=0;
	var sign="";//양수[],음수[-]
	numData=num;
	
	numData = numData.toString().replace(/\$|\,/g,'');
	
	if(isNaN(numData)) {
		numData = "";
		return numData;
	}
	if(numData.substring(0,1)=="-"){
		sign="-";
		numData=numData.substring(1);
	}
	
	
	//소숫점 및 "000.." 제거
	for(var i=0;i<numData.length;i++){
		if(numData.charAt(i)!='0'){
			break;
		}
		startRealNum++;
	}
	
	if(numData.length!=1&&startRealNum>0){
		if(numData.charAt(startRealNum)=='.'){
			numData = numData.substring(startRealNum-1);
		}else{
			numData = numData.substring(startRealNum);
		}
	}
	
	//소숫점 제거
	if(numData.charAt(0)=="."){
		numData="0."+numData.substring(1);
	}
	
	tmpNum=numData.split('.');
	if(tmpNum.length==1){
		numData=tmpNum[0];
		cents="";
	}else if(tmpNum.length==2){
		numData	=tmpNum[0];
		cents	=tmpNum[1];
	}else{
		return "";
	}

	for (var i = 0; i < Math.floor((numData.length-(1+i))/3); i++)
		numData = numData.substring(0,numData.length-(4*i+3))+','+numData.substring(numData.length-(4*i+3));

	if(cents==""){
		return sign+numData;
	}else{
		return sign+(numData + "." + cents);
	}
}

//우편 형식
function formatZip(zip) {
	var zipData="";

	zipData=zip;

	zipData = replace(zipData.toString(),"-","");
	if(zipData==""){
		return "";
	}
	if(isNaN(zipData)||zipData.length!=6) {
		zipData = "";
		return zipData;
	}
	zipData=zipData.substring(0,3)+"-"+zipData.substring(3,6);
	return zipData;
}

//주민번호 체크 로직
function juminCheck(jumin_no) {
   	var jumin1,jumin2,a,b,c,d,e,f,g,h,i,j,k,l,sum,pivot,modulus,endnumber;
   	var str_jumin1 =jumin_no.substring(0,6);
   	var str_jumin2 =jumin_no.substring(6,13);
  
	jumin1 = str_jumin1;
	jumin2 = str_jumin2;
       
		a = jumin1.substring(0, 1);
		aa = a * 2;
		b = jumin1.substring(1, 2);
		bb = b * 3;
		c = jumin1.substring(2, 3);
		cc = c * 4;
		d = jumin1.substring(3, 4);
		dd = d * 5;
		e = jumin1.substring(4, 5);
		ee = e * 6;
		f = jumin1.substring(5, 6);
		ff = f * 7;
   
		g = jumin2.substring(0, 1);
		gg = g * 8;
		h = jumin2.substring(1, 2);
		hh = h * 9;
		i = jumin2.substring(2, 3);
		ii = i * 2;
		j = jumin2.substring(3, 4);
		jj = j * 3;
		k = jumin2.substring(4, 5);
		kk = k * 4;
		l = jumin2.substring(5, 6);
		ll = l * 5;
   
		pivot = jumin2.substring(6,7);
   
		sum = aa + bb + cc + dd + ee + ff + gg + hh + ii + jj + kk + ll;
		modulus = sum % 11;
		endnumber = 11 - modulus;
   
		if(endnumber == 11)
			endnumber = 1;
		else if(endnumber == 10)
			endnumber = 0;
		else 
			endnumber = endnumber;
     
		if (pivot != endnumber){
			return false;
		}else{
			return true;
		}	
	return true;
}

/* 주민등록번호 체크 */
function pinNoCheck(J1, J2) {
    if(J1 =="111111" || J2 =="1111118"){
    alert("올바른 형식의 주민등록 번호 13자리를 입력하세요")
	return false;
    }else{
	// 주민등록번호 1 ~ 6 자리까지의 처리
	// 주민등록번호에 숫자가 아닌 문자가 있을 때 처리
        for(i=0;i<J1.length;i++){
	    if (J1.charAt(i) >= 0 && J1.charAt(i) <= 9){
	    // 숫자면 값을 곱해 더한다.
		if(i == 0)
		    SUM = (i+2) * J1.charAt(i);
		else
		    SUM = SUM +(i+2) * J1.charAt(i);
	    }else{
		 // 숫자가 아닌 문자가 있을 때의 처리
		 alert("숫자값만 입력 가능합니다.");
		return false;
	    }
	}//end of for loop

	for(i=0;i<2;i++){
            // 주민등록번호 7 ~  8 자리까지의 처리
	    if(J2.charAt(i) >= 0 && J2.charAt(i) <= 9){
                SUM = SUM + (i+8) * J2.charAt(i);
	    }else{
		// 숫자가 아닌 문자가 있을 때의 처리
			alert("숫자값만 입력 가능합니다.");;
		return false;
	    }
	}

	for(i=2;i<6;i++){
	    // 주민등록번호 9 ~ 12 자리까지의 처리
	    if (J2.charAt(i) >= 0 && J2.charAt(i) <= 9) {
	        SUM = SUM + (i) * J2.charAt(i);
	    }else{
	        // 숫자가 아닌 문자가 있을 때의 처리
	        alert("숫자값만 입력 가능합니다.");
		return fale;
	    }
	}

	// 나머지 구하기
	var checkSUM = SUM % 11;
	// 나머지가 0 이면 10 을 설정
	if(checkSUM == 0){
	    var checkCODE = 10;
	    // 나머지가 1 이면 11 을 설정
	}else if(checkSUM ==1){
	    var checkCODE = 11;
	}else{
	    var checkCODE = checkSUM;
	}
	// 나머지를 11 에서 뺀다
	var check1 = 11 - checkCODE;
	if (J2.charAt(6) >= 0 && J2.charAt(6) <= 9) {
            var check2 = parseInt(J2.charAt(6))
	}else{
	    // 숫자가 아닌 문자가 있을 때의 처리
	    alert("숫자값만 입력 가능합니다.");
	}
	if(check1 != check2){
	    // 주민등록번호가 틀릴 때의 처리
	    alert("올바른 형식의 주민등록 번호 13자리를 입력하세요")
	    return false;
	}else{
	    return true;
	}
    }
}

function emailCheck(fieldValue){
    if(fieldValue != "" && (fieldValue.indexOf('@') == -1 || fieldValue.indexOf('.') == -1)){
	return false;
    }
    return true;
}

function URLCheck(fieldValue){
    if(fieldValue != "" &&fieldValue.indexOf('.') == -1){
	return false;
    }
    return true;
}

/* 전화번호 체크 ( '-' 포함) */
function phoneCheck(fieldValue){
    if(fieldValue != "" && fieldValue.indexOf('-')==-1){
    	return false;
    }else{
    	notN = false;
    	phones = fieldValue.split('-');
    	for(pc=0; pc < phones.length ; pc++){
    		if(isNaN(phones[pc])) {
    			notN = true;
    		}
    	}
    	if (notN == true || fieldValue.indexOf(".") >=0) {
    		return false;
    	}
    	return true;
    }
}

//Radio 체크
function radioChecked(obj) {
	if (obj.length > 1) {
		for(i=0; i <obj.length; i++){
			if(obj[i].checked){
				return obj[i].value;
			}
		}
		return false;
	}
	else {
		if(obj.checked){
			return obj.value;
		}
		return false;
	}
}

// 리셋
function resetObj(obj) {
	obj.value = "";
    obj.focus();
}

// return 공통 함수
function returnFalse(formField) {
    formField.focus();
    return false
}

// 엔터키 체크
function enterCheck() {
    if(event.keyCode==13) {
        return true;
    }
    return false;
}

// textArea의 특수문자를 replace하여 서버로 보냄
function sendTextAreaValue(str) {
	if (str == null)
	    return false;
	
	str = replaceCharacter(str, "<", "&lt");
	str = replaceCharacter(str, ">", "&gt");
	str = replaceCharacter(str, "\"", "&quot");
	str = replaceCharacter(str, "'", "&#039;");
	str = replaceCharacter(str, "\n", "&et");
	
	return str;
}

// textArea의 특수문자를 replace하여 보여줌
function receiveTextAreaValue(str) {
	if (str == null)
	    return false;
	
	str = replaceCharacter(str, "&lt", "<");
	str = replaceCharacter(str, "&gt", ">");
	str = replaceCharacter(str, "&quot", "\"");
	str = replaceCharacter(str, "&#039;", "'");
	str = replaceCharacter(str, "&et", "\n");
	
	return str;
}

/*=========================================* 
* 주민등록 여부를 확인한다.(외국인)
* 
* param : sID 입력문자열(주민번호 13자리)
*
* return : Boolean true이면 적합한 주민번호
/*=========================================*/
function isFgnSocialNO(ssn) {
	if ((ssn.charAt(6) == "5") || (ssn.charAt(6) == "6"))
	{
		birthYear = "19";
	}
	else if ((ssn.charAt(6) == "7") || (ssn.charAt(6) == "8"))
	{
		birthYear = "20";
	}
	else if ((ssn.charAt(6) == "9") || (ssn.charAt(6) == "0"))
	{
		birthYear = "18";
	}
	else
	{
		return false;
	}  
	
	birthYear += ssn.substr(0, 2);
	birthMonth = ssn.substr(2, 2) - 1;
	birthDate = ssn.substr(4, 2);
	birth = new Date(birthYear, birthMonth, birthDate);
	
	if ( birth.getYear() % 100 != ssn.substr(0, 2) ||
		 birth.getMonth() != birthMonth ||
		 birth.getDate() != birthDate) {
	
		return false;
	}
	
	var sum = 0;
	var odd = 0;
	
	buf = new Array(13);
	for (i = 0; i < 13; i++) buf[i] = parseInt(ssn.charAt(i));
	
	odd = buf[7]*10 + buf[8];
	
	if (odd%2 != 0) {
		return false;
	}
	
	if ((buf[11] != 6)&&(buf[11] != 7)&&(buf[11] != 8)&&(buf[11] != 9)) {
		return false;
	}
	
	multipliers = [2,3,4,5,6,7,8,9,2,3,4,5];
	for (i = 0, sum = 0; i < 12; i++) sum += (buf[i] *= multipliers[i]);
	
	
	sum=11-(sum%11);
	
	if (sum>=10) sum-=10;
	
	sum += 2;
	
	if (sum>=10) sum-=10;
	
	if ( sum != buf[12]) {
		return false;
	} else {
		return true;
	}
	
	return true;
}



function getToday(){


	var now = new Date();
	var tmon, tday = 0; 
	if (now.getMonth()+1 < 10){
		mon = "0" + (now.getMonth()+1);
	}else{
		mon = "" + (now.getMonth()+1);
	}
	if (now.getDate()+1 < 10){
		tday = "0" + (now.getDate());
	}else{
		tday = "" + (now.getDate());
	}
	
	return  now.getYear() + "-" + mon + "-"  +tday;
	
}

//////////////////////////////////////////////////////////////////////////////
//달력 띄울때
//////////////////////////////////////////////////////////////////////////////
function pubOpenCalendar(url, id) {
var winOpt, oWin

winOpt = "width=160,height=158,resizable=no,status=no";

winOpt = winOpt + ",top=" + event.screenY + ",left=" + event.screenX;

oWin = window.open(url, id, winOpt);
oWin.focus();
}

//-------------------------------------------------------------------------------------
//숫자만 입력받기
//<INPUT type="text" name="textbox" onkeypress="pubOnlyNumber();">
//-------------------------------------------------------------------------------------
function pubOnlyNumber() {
if (!(event.keyCode >= 48 && event.keyCode <= 57))
	event.returnValue = false;
}

function addCommas(strValue)
{	

strValue = strValue.split(",").join("");

var objRegExp = new RegExp('(-?[0-9]+)([0-9]{3})'); 
while(objRegExp.test(strValue)) 
{
	strValue = strValue.replace(objRegExp, '$1,$2');
} 
return strValue;
}

function replaceCharacter(oOrg, sChar1, sChar2)
{
	if (oOrg==null) 	return false;
	if (sChar2==null) 	sChar2 = "";

	var regExpression = new RegExp(sChar1,"g")
	if (typeof(oOrg) == "object")
	{
		oOrg.value = oOrg.value.replace(regExpression, sChar2);
		oOrg.select();
		return false;
	}
	else
	{
		return oOrg.replace(regExpression, sChar2);
	}
}
function chgLinefeed(a){
	
	a=a.replace(/\\/gi,"\\\\");
	//a=a.replace(/\r\n/gi,"ysc1ysc");
	a=a.replace(/\n/gi,"\\n");
	a=a.replace(/\r/gi,"\\r");
	
	return a;
}
//====================================================================================
//리스트에서 체크박스 전체선택 (반전)
//
//oAllChkBox : 전체선택 체크박스
//oChkBox    : 처리될 체크박스
//====================================================================================
function chk_all_chkbox(oAllChkBox, oChkBox) {
 var bln_checked = oAllChkBox;
 if (eval(oChkBox)) {
     if (eval(oChkBox.length)) {
         var chkbox_count = oChkBox.length;

         for (var i = 0; i < chkbox_count; i++)
             oChkBox[i].checked = bln_checked;
     }
     else
         oChkBox.checked = bln_checked;
 }
}

function chk_val_chkbox(oChkBox){
	var result = "";
	if (eval(oChkBox)) {
	     if (eval(oChkBox.length)) {
	         var chkbox_count = oChkBox.length;

	         for (var i = 0; i < chkbox_count; i++)
	        	 if(oChkBox[i].checked)
	             result += oChkBox[i].value+",";
	         result = result.substring(0,result.length-1);
	     }
	     else
	    	 if(oChkBox.checked)
	    	 result += oChkBox.value;
	 }
	return result;
}
//달력 공통 팝업창 param1 = zip1,zip2 일경우 zip param2 = addr
function openCalendar(oldzip,oldaddr,newzip,newaddr,newno){
	//var cal = window.open(ctxRoot+'/ersmPop01.do?oldzip='+oldzip+'&newzip='+newzip+"&oldaddr="+oldaddr+"&newaddr="+newaddr+"&newno="+newno,'ersmCalendar','width=840,height=560, scrollbars=no');
	//var cal = window.open(ctxRoot+'/road/jibun.jsp?oldzip='+oldzip+'&newzip='+newzip+"&oldaddr="+oldaddr+"&newaddr="+newaddr+"&newno="+newno,'ersmCalendar','width=440,height=660, scrollbars=no');
	var cal = window.open(ctxRoot+'/road/juso.jsp?oldzip='+oldzip+'&newzip='+newzip+"&oldaddr="+oldaddr+"&newaddr="+newaddr+"&newno="+newno,'ersmCalendar','width=640,height=760, scrollbars=yes');
	return false;
}

//업종코드검색 공통 팝업창 param1 = code param2 = name
function openGroupsearch(code,name){
	var ersmGroupsearch = window.open(ctxRoot+'/ersmPop02.do?code='+code+'&name='+name,'ersmGroupsearch','width=450,height=350, scrollbars=no');
	return false;
}

//업종코드검색 공통 팝업창 param1 = code param2 = name bagicyyyy = currentyear
function openGroupsearch2(code,name){
	var ersmGroupsearch = window.open(ctxRoot+'/selectIndust.do?code='+code+'&name='+name,'ersmGroupsearch','width=450,height=350, scrollbars=no');
	return false;
}

//전년도 사업장 불러오기 공통 팝업창
function openCompany(){
	var openCompany = window.open(ctxRoot+'/ersmPop05.do','openCompany','width=750,height=350, scrollbars=yes');
	return false;
}

//전년도 장애인명부 불러오기 공통 팝업창
function openPreDisabled(){
	var openPreDisabled = window.open(ctxRoot+'/ersmPop04.do','openPreDisabled','width=980,height=500, scrollbars=yes');
	return false;
}

//전년도 고용계획 불러오기 공통 팝업창
function openPlanSearch(){
	var openPlanSearch = window.open(ctxRoot+'/ersmPrePlanSearch.do','openPlanSearch','width=750,height=350, scrollbars=yes');
	return false;
}

//장애유형 선택 공통 팝업창
function opendisGradeSelect(){
	var opendisGradeSelect = window.open(ctxRoot+'/ersmDisabledGradeSelect.do','opendisGradeSelect','width=620,height=640, scrollbars=yes');
	return false;
}
//업무 시스템 공통 팝업창
function openEsingo( url ){
	  var esingo = window.open("","esingo","width=997,height=700, scrollbars=yes");
	  $("#esingo").attr("target","esingo");
	  $("#esingo").attr("action",url);
	  $("#esingo").submit();
	  return false;
}